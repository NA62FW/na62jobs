#!/usr/bin/env python3
'''

@author:     Nicolas Lurkin
'''
import sys
from argparse import ArgumentParser, RawDescriptionHelpFormatter
from pathlib import Path

from na62jobs.reader import task_builder
from na62jobs.exceptions import ConfigError

# The script runs these different steps.
# Each has a well defined list of tasks
#  - The "build" step reads the configuration file and prepares a cache directory with the job definition.
#      All later steps will use the cache directory as a starting point. It is a preliminary
#      step required for all further steps
#  - The "prepare" step fills the cache with all the files necessary for the submission, and defines the list of jobs to run
#  - The "submit" step does the actually condor submission according to the latest list of jobs to run
#  - The "check" step checks and logs the status of all the submitted jobs: running, completed, failed, gone.
#      Also checks existence of output file for the job (i.e. a completed job can go to gone if the output disappeared)
#  - The "resubmit" step is equal to the "prepare" step -> Generalize "prepare" to take into account inputs successfully completed
#  - The "rebuild" step reads the provided config file and updates some of the cache.
#      Only some sections can be updated, will complain if other sections are inconsistent. The logic follows that
#      existing output cannot be invalidated (i.e. existing would still be the same with the updated config)
#      The allowed updates are:
#       - input: recheck existing input for new files that appeared, recheck for new samples.
#                Things like "version" and "filter" cannot be updated (can add to a sample, not change it).
#       - output: If the previous output directory has been moved. If not, all the old jobs will change status to "gone" and will be re-run.
#                Things like "what" cannot be changed
#       - temporary: If previous temporary directory has been moved. Else will complaint as this step is based on the cache.
#      The non-allowed updates are:
#       - process: The process cannot be changed as the existing output are not output of the same process
#  - The "print" step reads the cache and prints the summary (for bookkeeping/information purposes)
#  - The "clear" step just deletes the cache

def load_tasks_from_config(config_path, req_tasks, print_summary):
    tasks = {_.name: _ for _ in task_builder.load_yaml(config_path, print_summary)}
    tasks_to_run = []
    if len(tasks) == 0:
        raise ConfigError("No tasks found in the configuration file")
    if req_tasks is None:
        if len(tasks) > 1:
            raise ConfigError(
                "Multiple tasks found in the configuration file, but --tasks is not specified")
        else:
            tasks_to_run = [next(iter(tasks.values()))]
    else:
        for task in req_tasks:
            if task in tasks:
                tasks_to_run.append(tasks[task])
            else:
                raise ConfigError(
                    f"Task {task} not found in the configuration file")
    return tasks_to_run


def load_tasks_from_dir(job_dir, req_tasks):
    if not job_dir.exists():
        raise ConfigError("Specified job_dir does not exist")
    tasks = {}
    for task_file in job_dir.glob("*.json"):
        tasks[task_file.stem] = task_builder.build_task_from_json(task_file)

    tasks_to_run = []
    if len(tasks) == 0:
        raise ConfigError("No tasks found in the job_dir")
    if req_tasks is None:
        if len(tasks) > 1:
            raise ConfigError(
                "Multiple tasks found in the job_dir, but --tasks is not specified")
        else:
            tasks_to_run = [next(iter(tasks.values()))]
    else:
        for task in req_tasks:
            if task in tasks:
                tasks_to_run.append(tasks[task])
            else:
                raise ConfigError(
                    f"Task {task} not found in the configuration file")
    return tasks_to_run


def call_load_tasks(args, print_summary=False):
    if args.config is not None:
        tasks_to_run = load_tasks_from_config(args.config, args.tasks, print_summary)
    else:
        tasks_to_run = load_tasks_from_dir(args.job_dir, args.tasks)
    return tasks_to_run


def cmd_build(args):
    for task in load_tasks_from_config(args.config, args.tasks, print_summary=True):
        task.build()


def cmd_rebuild(args):
    for task in call_load_tasks(args):
        task.load_cache(check_consistency=True)
        task.rebuild()


def cmd_print(args):
    for task in call_load_tasks(args):
        task.load_cache(check_consistency=False)
        task.summary()
        if args.failed:
            task.detail_failed()


def cmd_prepare(args):
    for task in call_load_tasks(args):
        task.load_cache(check_consistency=False)
        task.summary(False)
        if args.split_runs:
            task.split_runs()
        if args.merge:
            task.create_merge_jobs(args.force)
            task.prepare_merge_jobs(operating_system=args.os)
        else:
            task.prepare_submission(args.erase_outdir, args.ignore_outdir, job_flavour=args.flavour, operating_system=args.os)


def cmd_submit(args):
    for task in call_load_tasks(args):
        task.load_cache(check_consistency=False)
        task.summary(print_processing=False)
        if args.reset_failed:
            task.reset_failed_files()
        if args.reset_gone:
            task.reset_gone_files()
        task.submit(job_limit=args.job_limit, batch_name=args.batch_name)
        task.summary_processing()


def cmd_check(args):
    for task in call_load_tasks(args):
        task.load_cache(check_consistency=False)
        task.summary(print_processing=False)
        task.check(args.check_gone)
        print()
        task.summary_processing()


def cmd_clear(args):
    for task in call_load_tasks(args):
        task.clear()


steps_values = (("build",    cmd_build,    "Read the configuration file and build the cache", []),
                ("rebuild",  cmd_rebuild,  "Read the configuration and update an existing cache", []),
                ("print",    cmd_print,    "Read the cache and print the summary",
                    [("--failed", {"help": "Print the list of failed jobs. You can then investigate the reason of the failure by looking in the corresponding output and error files", "action": "store_true", "default": False}),
                    ]),
                ("prepare",  cmd_prepare,  "Prepare all the files and the list of jobs for the submissions",
                    [("--erase-outdir", {"help": "Erase the output directory if not empty", "action": "store_true", "default": False}),
                     ("--ignore-outdir", {"help": "Ignore the output directory not being empty", "action": "store_true", "default": False}),
                     ("--split-runs", {"help": "Do not merge runs in processing. Each output file is placed in a dedicated run directory." \
                                       " This option is effective only for 'Samples' and 'Runs' input.", "action": "store_true", "default": False}),
                     ("--merge", {"help": "Prepare merge jobs. All samples must be completed", "action": "store_true", "default": False}),
                     ("--force", {"help": "In combination with --merge. Force to run the merge job even if the processing of the samples is not complete", "action": "store_true", "default": False}),
                     ("--flavour", {"help": "HTCondor flavour (job length). Default: tomorrow", "default": "tomorrow"}),
                     ("--os", {"help": "Requested operating system. Possible values: CentOS7, AlmaLinux9 (Default)", "default": "AlmaLinux9"})
                     ]),
                ("submit",   cmd_submit,   "Submit on the batch system",
                    [("--job-limit", {"help": "Specify a maximum number of jobs to launch simultaneously", "type": int}),
                     ("--reset-failed", {"help": "Reset failed jobs and reinclude them in the submission", "action": "store_true", "default": False}),
                     ("--reset-gone", {"help": "Reset jobs whose output has beer removed and reinclude them in the submission", "action": "store_true", "default": False}),
                     ("--batch_name", {"help": "Provide a batch_bame that will be assoaicated with the jobs (for e.g. condor_q)", "default": None}),
                     ]),
                ("check",    cmd_check,    "Check status of submitted jobs",
                    [("--check-gone", {"help": "Check output of previously completed jobs to check if their output is still present", "action": "store_true", "default": False}),
                    ]),
                ("clear",    cmd_clear,    "Delete the cache", []),
                )


def main(argv=None):
    '''Command line options.'''

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    # Setup argument parser
    parser = ArgumentParser(
        description="", formatter_class=RawDescriptionHelpFormatter)

    subparser = parser.add_subparsers(
        description="NA62 jobs submitter step to run")
    # Build arguments
    build_parser = ArgumentParser(add_help=False)
    build_parser.add_argument("config", help="Configuration file to load")
    build_parser.add_argument("--tasks", help="List of tasks to run", nargs="+")

    # From cache arguments
    cache_parser = ArgumentParser(add_help=False)
    group = cache_parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--job-dir", help="Job directory from which to retrieve jobs", type=Path)
    group.add_argument("--config", help="Config file where the job directory will be read")
    cache_parser.add_argument("--tasks", help="List of tasks to run", nargs="+")

    for step in steps_values:
        if "build" in step[0]:
            parent = build_parser
        else:
            parent = cache_parser
        this_parser = subparser.add_parser(step[0], help=step[2], parents=[parent])
        this_parser.set_defaults(func=step[1])
        for arg, kwargs in step[3]:
            this_parser.add_argument(arg, **kwargs)

    # Process arguments
    args = parser.parse_args()
    if not hasattr(args, "func"):
        parser.print_help()
        return 0
    return args.func(args)

if __name__ == "__main__":
    sys.exit(main())
