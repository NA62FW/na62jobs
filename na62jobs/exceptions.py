class SampleError(Exception):
    pass


class ConfigError(Exception):
    pass


class ConsistencyError(Exception):
    pass


class PersistencyError(Exception):
    pass

class StepError(Exception):
    pass

class BatchSubmitterError(Exception):
    def __init__(self, message, log):
        super().__init__(message)
        self.full_log = log

    def __str__(self):
        return (super().__str__() + "\n=== Full submitter log:\n" + "".join(self.full_log))

class BatchSubmitterCheckFailed(Exception):
    pass