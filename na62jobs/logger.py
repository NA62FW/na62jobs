

def step(msg: str) -> None:
    print(f"==> {msg}")


def title(msg: str) -> None:
    print(f"--- {msg}")


def item(msg: str) -> None:
    print(f"{'':>5}-> {msg}")


def action(msg: str) -> None:
    print(f"{'':>5}!!! {msg}")


def subitem(msg: str) -> None:
    indented_line(msg, 8)


def indented_line(msg: str, indent_size: int) -> None:
    indent = ' ' * indent_size
    print("\n".join([f"{indent}{_}" for _ in msg.split("\n")]))
