import pytest
from . import fake_eos
from ..reader import inputClasses, utils

# Do not read from EOS, provide a fake test FS
inputClasses.EOSPath = fake_eos.EOSPath
inputClasses.eos_experiment = fake_eos.EOSPath(
    "/eos/fake_fs/", mgm_url="xroot://eosna62.cern.ch/")
inputClasses.eos_data = inputClasses.eos_experiment / "Data"
inputClasses.eos_mc = inputClasses.eos_experiment / "MC"
utils.EOSPath = fake_eos.EOSPath

from ..exceptions import ConfigError, SampleError, ConsistencyError
from ..reader.inputClasses import InputRecord, Properties, InputStatus, RangeInput, MergeInput, GenericInput, OfficialInput, ListInput, RunsInput, MCSampleInput, DataSampleInput

def input_to_val(inputlist):
    return [int(k.properties["value"]) if "value" in k.properties else str(k.input_value) for k in inputlist]


class TestInputRecord:
    @pytest.fixture
    def record1(self):
        return InputRecord("/test/a", Properties({"p1": "v1", "subdir": "sub"}))

    def test_change_allowed(self, record1):
        record1.set_job_id(10)
        record1.set_subdir("test")
        assert(record1.job_id == 10)
        assert(record1.properties["subdir"] == "test")

    def test_change_notallowed(self, record1):
        record1.status = InputStatus.COMPLETED
        record1.set_job_id(20)
        record1.set_subdir("test")
        assert(record1.job_id == -1)
        assert(record1.properties["subdir"] == "sub")


class NA62FS:
    def _fill_fs(self):
        self._init_fs = fake_eos.dir_content
        fake_eos.dir_content = {"/": {"eos": {"fake_fs": {"Data":
                                                          {"2018": {
                                                              "2018E-v3.6.0": {"Run009001.CTRL.p.v3.6.0-01_f.v3.6.0-01.list": -1, "Run009001.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": -1},
                                                              "2018F-v3.6.0": {"Run009001.CTRL.p.v3.6.0-01_f.v3.6.0-01.list": -1, "Run009001.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": -1},
                                                          },
                                                              "2022": {
                                                              "2022A-v3.6.0": {"Run011934.CTRL.p.v3.6.0-01_f.v3.6.0-01.list": -1, "Run011934.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": ["file1.root", "file2.root"], "Run011935.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": ["file3.root", "file1.root"]},
                                                              "2022A-v3.5.0": {"Run011934.CTRL.p.v3.5.0-01_f.v3.5.0-01.list": -1, "Run011934.1TPNN.p.v3.5.0-01_f.v3.5.0-01.list": -1, "Run011935.1TPNN.p.v3.5.0-01_f.v3.5.0-01.list": -1},
                                                              "2022B-v3.5.0": {"Run011950.CTRL.p.v3.6.0-01_f.v3.6.0-01.list": -1, "Run011950.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": -1, "Run011950.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list": -1},
                                                          }}
                                                          }}}}

    def _reset_fs(self):
        fake_eos.dir_content = self._init_fs


class TestGenericInput:
    @pytest.fixture
    def test_input(self):
        i = GenericInput()
        g1 = [InputRecord("", serialized_dict={"input_value": "file1.root", "job_id": 1, "properties": {"value": 5}, "status": InputStatus.RUNNING}),
              InputRecord("", serialized_dict={"input_value": "file2.root", "job_id": 2, "properties": {
                          "subdir": "test"}, "status": InputStatus.FAILED}),
              InputRecord("", serialized_dict={
                          "input_value": "file3.root", "job_id": -1, "properties": {}, "status": InputStatus.NEEDED}),
              ]
        g2 = [InputRecord("", serialized_dict={"input_value": "file1.root", "job_id": 3, "properties": {}, "status": InputStatus.GONE}),
              InputRecord("", serialized_dict={"input_value": "file2.root", "job_id": 4, "properties": {
                          "subdir": "test"}, "status": InputStatus.FAILED}),
              InputRecord("", serialized_dict={
                          "input_value": "/eos/user/file3.root", "job_id": 3, "properties": {}, "status": InputStatus.NEEDED}),
              InputRecord("", serialized_dict={"input_value": "/eos/experiment/na62/file4.root",
                                               "job_id": 10, "properties": {}, "status": InputStatus.COMPLETED}),
              ]
        i.list_inputs_groups["g1"] = g1
        i.list_inputs_groups["g2"] = g2
        i.list_inputs_flat = g1 + g2
        return i

    def test_len_lists(self, test_input):
        assert(len(test_input) == 7)

    def test_update_files_status(self, test_input):
        test_input.update_files_status(
            None, InputStatus.NEEDED, InputStatus.RUNNING)
        assert(len(test_input.get_files(with_status=InputStatus.RUNNING)) == 3 and len(
            test_input.get_files(with_status=InputStatus.NEEDED)) == 0)
        test_input.update_files_status(
            None, [InputStatus.GONE, InputStatus.FAILED], InputStatus.NEEDED)
        assert(len(test_input.get_files(with_status=InputStatus.GONE)) == 0 and len(test_input.get_files(
            with_status=InputStatus.FAILED)) == 0 and len(test_input.get_files(with_status=InputStatus.NEEDED)) == 3)
        test_input.update_files_status(
            [3, 4], InputStatus.NEEDED, InputStatus.RUNNING)
        assert(len(test_input.get_files(with_status=InputStatus.RUNNING)) == 5)

    def test_get_files(self, test_input):
        assert(len(test_input.get_files(with_status=InputStatus.NEEDED)) == 2)
        assert(len(test_input.get_files(with_job_id=[3, 1])) == 3)
        assert(len(test_input.get_files(
            with_status=InputStatus.FAILED, with_job_id=[2])) == 1)

    def test_is_complete(self, test_input):
        assert(test_input.is_complete() == False)
        test_input.update_files_status(
            None, [InputStatus.NEEDED, InputStatus.FAILED, InputStatus.GONE, InputStatus.RUNNING], InputStatus.COMPLETED)
        assert(test_input.is_complete() == True)

    def test_get_fs_set(self, test_input):
        assert(test_input.get_fs_set() == {'local', 'eos_na62', 'eos_user'})

    def test_check_fs(self, test_input):
        with pytest.raises(ConfigError):
            test_input.check_fs()

    def test_map(self, test_input):
        test_input.update_files_status(
            None, [InputStatus.COMPLETED, InputStatus.FAILED, InputStatus.GONE, InputStatus.RUNNING], InputStatus.NEEDED)
        test_input.reset_inputs_job_id()
        mapped = test_input.map(3)
        assert(len(mapped) == 4)
        for chunk in mapped:
            assert(len(set([_.properties.str_id() for _ in chunk])) == 1)

    def test_get_needed_chunks(self, test_input):
        assert(test_input.get_needed_chunks() == [(-1, {}), (3, {})])

    def test_get_all_properties(self, test_input):
        assert(test_input.get_all_properties() == [
               ('value_5', {'value': 5}), ('subdir_test', {'subdir': 'test'}), ('', {})])


class TestRangeInput:
    @pytest.fixture
    def test_input(self):
        return RangeInput([0, 10, 1])

    def test_creation_range_single(self):
        test_single = RangeInput([10])
        assert(test_single.first == 0)
        assert(test_single.last == 10)
        assert(test_single.step == 1)

    def test_creation_range_extrema(self):
        test_single = RangeInput([5, 10])
        assert(test_single.first == 5)
        assert(test_single.last == 10)
        assert(test_single.step == 1)

    def test_creation_range_step(self):
        test_single = RangeInput([0, 10, 2])
        assert(test_single.first == 0)
        assert(test_single.last == 10)
        assert(test_single.step == 2)

    def test_creation_range_invert(self):
        test_single = RangeInput([10, 0])
        assert(test_single.first == 10)
        assert(test_single.last == 0)
        assert(test_single.step == -1)

    def test_creation_range_invalid(self):
        with pytest.raises(ConfigError):
            RangeInput([0, 0, 0, 0])

    def test_resolve(self, test_input):
        assert(test_input.resolve_sample() == 11)
        assert(sorted(input_to_val(test_input.list_inputs_flat))
               == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

    def test_resolve_update(self, test_input):
        test_input.resolve_sample()
        test_input.last = 12
        assert(test_input.resolve_sample(update=True) == 13)
        assert(sorted(input_to_val(test_input.list_inputs_flat))
               == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])


class TestMergeInput:
    @pytest.fixture
    def test_input(self):
        return MergeInput({"path": "xroot://eosna62.cern.ch//dir1", "subdirs": ["subdir1", "subdir2"]})

    @pytest.fixture
    def test_wrong_path(self):
        return MergeInput({"path": "xroot://eosna62.cern.ch//somepath", "subdirs": ["subdir1", "subdir2"]})

    def test_creation(self):
        m = MergeInput({"path": "xroot://eosna62.cern.ch//dir1",
                        "subdirs": ["subdir1", "subdir2"]})
        assert(str(m.src_path) == "/dir1")
        assert(m.subdirs == ["subdir1", "subdir2"])

    def test_resolve_path_not_existing(self, test_wrong_path):
        with pytest.raises(ConfigError):
            test_wrong_path.resolve_sample()

    def test_resolve(self, test_input):
        assert(test_input.resolve_sample() == 4)
        assert(input_to_val(test_input.list_inputs_flat) == [
               '/dir1/subdir1/file1.root', '/dir1/subdir2/file1.root', '/dir1/subdir1/file2.root', '/dir1/subdir2/file2.root'])
        assert(list(test_input.list_inputs_groups.keys())
               == ["/dir1/subdir1", "/dir1/subdir2"])
        assert(len(test_input.list_inputs_groups["/dir1/subdir1"]) == 2 and len(
            test_input.list_inputs_groups["/dir1/subdir2"]) == 2)

    def test_resolve_update(self, test_input):
        with pytest.raises(SampleError):
            test_input.subdirs.append("test")
            test_input.resolve_sample(update=True)


class TestOfficialInput(NA62FS):
    @pytest.fixture
    def test_input(self):
        i = OfficialInput({"version": "v3.6.0", "filter": "1TPNN"})
        return i

    @pytest.fixture
    def test_paths(self):
        valid_list = ['Run011934.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list',
                      'Run011935.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list']
        src_path = fake_eos.EOSPath(
            "/eos/fake_fs/Data/2022/2022A-v3.6.0", mgm_url="xroot://eosna62.cern.ch/")
        return valid_list, src_path

    def test_check_consistency(self, test_input):
        with pytest.raises(ConsistencyError):
            test_input.check_consistency({"filter": "another"})
        test_input.check_consistency({"filter": "another"}, False)

    def test_check_versions(self, test_input):
        self._fill_fs()
        assert(test_input.check_versions("2022", "2022A") == "v3.6.0")
        with pytest.raises(SampleError):
            test_input.check_versions("2023", "2022A")
        with pytest.raises(SampleError):
            test_input.check_versions("2022", "2022C")
        with pytest.raises(SampleError):
            # Not sure about the result of that one
            test_input.version = "latest-common"
            test_input.found_version = "v3.4.0"
            test_input.check_versions("2022", "2022A")
        self._reset_fs()

    def test_get_lists(self, test_input):
        self._fill_fs()
        with pytest.raises(SampleError):  # Invalid year
            test_input.get_lists("2024", "2022A", "v3.6.0", None)
        with pytest.raises(SampleError):  # Invalid period
            test_input.get_lists("2022", "2022C", "v3.6.0", None)
        with pytest.raises(SampleError):  # Invalid version
            test_input.get_lists("2022", "2022A", "v3.6.1", None)
        with pytest.raises(SampleError):  # Run does not exist
            test_input.get_lists("2022", "2022A", "v3.6.0", [11936])

        # Correct for any run
        assert(test_input.get_lists("2022", "2022A", "v3.6.0", None)[0] == [
               'Run011934.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list', 'Run011935.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list'])
        # Correct for specific run
        assert(test_input.get_lists("2022", "2022A", "v3.6.0", [11934])[
               0] == ['Run011934.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list'])
        self._reset_fs()

    def test_extract_files_from_lists(self, test_input, test_paths):
        self._fill_fs()
        valid_list, src_path = test_paths
        test_input.extract_files_from_lists(valid_list, "2022A", src_path)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file2.root', 'file3.root', 'file1.root'])
        self._reset_fs()

    def test_update_files_from_lists(self, test_input, test_paths):
        self._fill_fs()
        valid_list, src_path = test_paths
        test_input.extract_files_from_lists(valid_list, "2022A", src_path)
        # Add an entry in the list
        (src_path / valid_list[0])._get_().append("file4.root")
        test_input.update_files_from_lists(valid_list, "2022A", src_path)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file2.root', 'file3.root', 'file1.root', 'file4.root'])
        self._reset_fs()


class TestListInput:
    @pytest.fixture
    def test_input(self):
        i = ListInput("/eos/user/mylist.lst")
        i.src_path = fake_eos.EOSPath("/eos/user/mylist.lst")
        return i

    def _fill_fs(self):
        self._init_fs = fake_eos.dir_content
        fake_eos.dir_content = {"/": {"eos": {"user": {"mylist.lst": [
            "/eos/user/file1.root", "/eos/user/file2.root", "/eos/user/file3.root"]}}}}

    def _reset_fs(self):
        fake_eos.dir_content = self._init_fs

    def test_resolve_sample(self, test_input):
        self._fill_fs()
        assert(test_input.resolve_sample() == 3)
        assert(input_to_val(test_input.list_inputs_flat) == [
               "/eos/user/file1.root", "/eos/user/file2.root", "/eos/user/file3.root"])
        assert(list(test_input.list_inputs_groups.keys())
               == ['/eos/user/mylist.lst'])
        self._reset_fs()

    def test_resolve_sample_non_existing(self, test_input):
        with pytest.raises(ConfigError):
            test_input.resolve_sample()

    def test_resolve_sample_update(self, test_input):
        self._fill_fs()
        test_input.resolve_sample()
        test_input.src_path._get_().append("/eos/user/file4.root")
        assert(test_input.resolve_sample(True) == 4)
        assert(input_to_val(test_input.list_inputs_flat) == [
               "/eos/user/file1.root", "/eos/user/file2.root", "/eos/user/file3.root", "/eos/user/file4.root"])
        assert(list(test_input.list_inputs_groups.keys())
               == ['/eos/user/mylist.lst'])
        self._reset_fs()


class TestRunsInput(NA62FS):
    @pytest.fixture
    def test_dict(self):
        return {"name": "myruns", "numbers": [11934, 11935], "version": "v3.6.0", "filter": "1TPNN"}

    @pytest.fixture
    def test_input(self, test_dict):
        return RunsInput(test_dict)

    def test_check_consistency(self, test_input, test_dict):
        test_dict["run_numbers"] = 3001
        with pytest.raises(ConsistencyError):
            test_input.check_consistency(test_dict, True)
        test_dict["run_numbers"] = [11934, 11935]
        test_dict["sample_name"] = "myotherruns"
        with pytest.raises(ConsistencyError):
            test_input.check_consistency(test_dict, True)

    def test_build_cache(self, test_input):
        self._fill_fs()
        print(test_input.build_cache())
        assert(test_input.build_cache() == {('2018', '2018E'): {9001}, ('2018', '2018F'): {
               9001}, ('2022', '2022A'): {11934, 11935}, ('2022', '2022B'): {11950}})
        self._reset_fs()

    def test_list_periods(self, test_input):
        self._fill_fs()
        assert(test_input.list_periods([11934]) == {('2022', '2022A')})
        with pytest.raises(ConfigError):
            test_input.list_periods([3000])
        with pytest.raises(ConfigError):
            test_input.list_periods([9001])
        self._reset_fs()

    def test_resolve_sample(self, test_input):
        self._fill_fs()
        assert(test_input.resolve_sample() == 4)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file1.root', 'file2.root', 'file3.root'])
        self._reset_fs()

    def test_resolve_sample_update(self, test_input):
        self._fill_fs()
        test_input.resolve_sample()
        (test_input.src_path / "2022" / "2022A-v3.6.0" /
         "Run011934.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list")._get_().append("file4.root")
        assert(test_input.resolve_sample(update=True) == 5)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file1.root', 'file2.root', 'file3.root', 'file4.root'])
        self._reset_fs()


class TestMCSampleInput(NA62FS):
    @pytest.fixture
    def test_dict(self):
        return {"name": "k3pi", "type": "mc", "run_number": 9001, "reco_version": "v3.6.0", "version": "v3.5.0"}

    @pytest.fixture
    def test_input(self, test_dict):
        return MCSampleInput(test_dict)

    def _fill_fs(self):
        super()._fill_fs()
        exp_dir = inputClasses.eos_experiment._get_()
        exp_dir["MC"] = {"v3.5.0": {
            "Reco_noov": {"k3pi.list": -1, "k3pi.capped.list": -1},
            "v3.5.0-v3.6.0": {"k3pi.Run009001.RECO.mc_noov.v3.6.0-01.list": -1, "k3pi.Run009001.RECO.mc.v3.6.0-01.list": ["file1.root", "file2.root"], "k3pi.Run013001.RECO.mc.v3.6.0-01.list": -1},
        },
            "v3.4.0-v3.6.0": {"k3pi.Run009001.RECO.mc_noov.v3.6.0-01.list": -1}}

    def test_check_consistency(self, test_input, test_dict):
        test_dict["mc_type"] = test_dict["type"]
        test_dict["filter"] = None
        test_dict["mc_type"] = "noov"
        test_input.check_consistency(test_dict, False)
        with pytest.raises(ConsistencyError):
            test_input.check_consistency(test_dict, True)
        test_dict["mc_type"] = test_dict["type"]
        test_dict["reco_version"] = "v3.5.0"
        with pytest.raises(ConsistencyError):
            test_input.check_consistency(test_dict, True)
        test_dict["reco_version"] = "v3.6.0"
        test_dict["run_number"] = 13000
        with pytest.raises(ConsistencyError):
            test_input.check_consistency(test_dict, True)

    def test_resolve_sample(self, test_input):
        self._fill_fs()
        assert(test_input.resolve_sample() == 2)
        assert(input_to_val(test_input.list_inputs_flat)
               == ['file1.root', 'file2.root'])

        # These should all fail
        with pytest.raises(SampleError):
            old_reco = test_input.reco_version
            test_input.reco_version = "v3.4.0"
            test_input.resolve_sample()
        test_input.reco_version = old_reco
        with pytest.raises(ConfigError):
            test_input.sample_name = "k2pi"
            test_input.resolve_sample()
        test_input.src_path._get_(
        )["k2pi.Run009001.RECO.mc.v3.6.0-01.list"] = -1
        test_input.src_path._get_(
        )["k2pi.Run009001.RECO.mc.v3.6.0-02.list"] = -1
        with pytest.raises(SampleError):
            test_input.resolve_sample()
        test_input.sample_name = "k3pi"
        with pytest.raises(SampleError):
            test_input.version = "v3.3.0"
            test_input.resolve_sample()
        test_input.mc_type = "random"
        with pytest.raises(ConfigError):
            test_input.resolve_sample()
        test_input.run_number = None
        with pytest.raises(ConfigError):
            test_input.resolve_sample()
        test_input.mc_type = None
        with pytest.raises(ConfigError):
            test_input.resolve_sample()

        self._reset_fs()

    def test_resolve_sample_mc_latest(self, test_input):
        self._fill_fs()
        test_input.version = "latest"
        assert(test_input.resolve_sample() == 2)
        assert(input_to_val(test_input.list_inputs_flat)
               == ['file1.root', 'file2.root'])
        self._reset_fs()

    def test_resolve_sample_reco_latest(self, test_input):
        self._fill_fs()
        test_input.reco_version = "latest"
        assert(test_input.resolve_sample() == 2)
        assert(input_to_val(test_input.list_inputs_flat)
               == ['file1.root', 'file2.root'])
        self._reset_fs()

    def test_resolve_sample_update(self, test_input):
        self._fill_fs()
        test_input.resolve_sample()
        test_input.src_path._get_(
        )["k3pi.Run009001.RECO.mc.v3.6.0-01.list"].append("file3.root")
        assert(test_input.resolve_sample(update=True) == 3)
        assert(input_to_val(test_input.list_inputs_flat) ==
               ['file1.root', 'file2.root', 'file3.root'])
        self._reset_fs()


class TestDataSampleInput(NA62FS):
    @pytest.fixture
    def test_dict(self):
        return {"name": "2022A", "filter": "1TPNN"}

    @pytest.fixture
    def test_input(self, test_dict):
        return DataSampleInput(test_dict)

    def test_resolve_sample(self, test_input):
        self._fill_fs()
        assert(test_input.resolve_sample() == 4)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file1.root', 'file2.root', 'file3.root'])
        with pytest.raises(ConfigError):
            test_input.filter = None
            test_input.resolve_sample()
        self._reset_fs()

    def test_resolve_sample_update(self, test_input):
        self._fill_fs()
        test_input.resolve_sample()
        test_input.src_path._get_(
        )["Run011935.1TPNN.p.v3.6.0-01_f.v3.6.0-01.list"].append("file4.root")
        assert(test_input.resolve_sample(update=True) == 5)
        assert(input_to_val(test_input.list_inputs_flat) == [
               'file1.root', 'file1.root', 'file2.root', 'file3.root', 'file4.root'])
        self._reset_fs()
