from ..reader.persistency import to_serializable, PersistencyMixin
from pathlib import Path
from ..reader.eos import EOSPath
import re
import pytest
from ..exceptions import PersistencyError

@pytest.fixture
def serializable_class():
    class MyClass(object):
        def serialize(self):
            return "success"
    return MyClass()

@pytest.fixture
def mixin_class():
    class MyClass(PersistencyMixin):
        MyClass_attr_to_save = ["success"]
        MyClass_pers_version = 1
        MyClass_attr_transform = {}

        def __init__(self):
            self.success = "success"

    return MyClass()

def test_to_serializable(serializable_class, mixin_class):
    assert(to_serializable(Path("/test/path"))=="/test/path")
    assert(to_serializable(EOSPath("/eos/experiment/na62/test/path"))=="xroot://eosna62.cern.ch//eos/experiment/na62/test/path")

    assert(to_serializable(serializable_class)=="success")
    assert(to_serializable((serializable_class, serializable_class))==("success", "success"))
    assert(to_serializable([serializable_class, serializable_class])==["success", "success"])
    assert(sorted(to_serializable({serializable_class, "v1"}))==sorted(["success", "v1"]))
    assert(to_serializable({"c1": serializable_class, serializable_class: "c2"})=={"c1": "success", "success": "c2"})
    assert(to_serializable(re.compile(".*"))=="re:.*")
    assert(to_serializable(mixin_class)=={'PersistencyMixin_pers_version': 0, 'success': 'success', 'MyClass_pers_version': 1})

class TestPersistencyMixin:

    class PersistencyTest(PersistencyMixin):
        PersistencyTest_attr_to_save = ["v1", "v2"]
        PersistencyTest_pers_version = 1
        PersistencyTest_attr_transform = {"v1": lambda x: f"{x}_app"}

        def __init__(self):
            self.v1 = "testv1"
            self.v2 = "testv2"

    @pytest.fixture
    def pers_class(self):
        return TestPersistencyMixin.PersistencyTest()


    def test_persistencymixin_has_persistency(self):
        class MyClass1(object):
            pass
        class MyClass2(PersistencyMixin):
            MyClass2_attr_to_save = []
        class MyClass3(PersistencyMixin):
            MyClass3_attr_to_save = []
            MyClass3_pers_version = 1
        assert(PersistencyMixin._has_persistency(TestPersistencyMixin.PersistencyTest, "PersistencyTest"))
        assert(PersistencyMixin._has_persistency(TestPersistencyMixin.PersistencyTest, "PersistencyTestWrong") is False)
        assert(PersistencyMixin._has_persistency(MyClass1, "MyClass1") is False)
        assert(PersistencyMixin._has_persistency(MyClass2, "MyClass2") is False)
        assert(PersistencyMixin._has_persistency(MyClass3, "MyClass3") is False)

    def test_persistencymixin_build_hierarchy(self):
        class MyClass(TestPersistencyMixin.PersistencyTest):
            MyClass_attr_to_save = []
            MyClass_pers_version = 1
            MyClass_attr_transform = {}
        assert(PersistencyMixin._build_pers_hierarchy(MyClass()) == [PersistencyMixin, TestPersistencyMixin.PersistencyTest, MyClass])

    def test_persistencymixin_create_persistency(self):
        @PersistencyMixin.create_persistency(attr_to_save=[], pers_version=1, attr_transform={})
        class MyClass(PersistencyMixin):
            pass
        assert(hasattr(MyClass, "MyClass_attr_to_save"))
        assert(hasattr(MyClass, "MyClass_pers_version"))
        assert(hasattr(MyClass, "MyClass_attr_transform"))

    def test_persistencymixin_save_class(self, pers_class):
        assert(pers_class.save_class(TestPersistencyMixin.PersistencyTest)=={'v1': 'testv1', 'PersistencyTest_pers_version': 1, 'v2': 'testv2'})

    def test_persistencymixin_save(self):
        class MyClass(TestPersistencyMixin.PersistencyTest):
            MyClass_attr_to_save = ["v2"]
            MyClass_pers_version = 1
            MyClass_attr_transform = {}
            def __init__(self):
                super().__init__()
                self.v2 = "v2"

        assert(MyClass().save()=={'PersistencyMixin_pers_version': 0, 'v1': 'testv1', 'PersistencyTest_pers_version': 1, 'v2': 'v2', 'MyClass_pers_version': 1})

    def test_persistencymixin_check_class_version(self, pers_class):
        with pytest.raises(PersistencyError):
            pers_class.check_class_version(TestPersistencyMixin.PersistencyTest, {"PersistencyTest_pers_version": 2})

    def test_persistencymixin_load_class(self, pers_class):
        p = pers_class
        p.load_class(TestPersistencyMixin.PersistencyTest, {"v1": "newvalue", "v2": "newvalue2"})
        assert(p.v1=="newvalue_app" and p.v2=="newvalue2")
        with pytest.raises(PersistencyError):
            p.load_class(TestPersistencyMixin.PersistencyTest, {})