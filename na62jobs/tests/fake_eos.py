from pathlib import Path
from typing import Iterator, Union
from ..reader.eos import EOSPath as RealEOS

# Initialize a fake FS with some default content
dir_content = {"/": {
    "dir1": {
        "subdir1": {"file1.root": -1, "file2.root": -1},
        "subdir2": {"file1.root": -1, "file2.root": -1}
    },
    "dir2": {"file1.root": -1, "file1.csv": -1},
    "dir3": {},
}
}


class EOSPath(RealEOS):
    def __init__(self, path: Union[str, Path], mgm_url: Union[str, None] = None):
        if mgm_url is None:
            if "fake_fs" in str(path):
                mgm_url = "xroot://eosna62.cern.ch"
        super().__init__(path, mgm_url)

    def _get_(self) -> Union[int, dict, str, None]:
        if str(self.path) == "/":
            return dir_content["/"]
        else:
            sub_content = self.parent._get_()
            name = str(self.name)
            if sub_content not in [None, -1] and name in sub_content:
                return sub_content[name]
            return None

    @property
    def parent(self):
        return EOSPath(self.path.parent, mgm_url=self.mgm_url)

    def __truediv__(self, other: str) -> 'EOSPath':
        return EOSPath(self.path / other, mgm_url=self.mgm_url)

    def iterdir(self, check_is_dir=False) -> Iterator['EOSPath']:
        content = self._get_()
        if content is None:
            return None
        if content == -1:
            return []
        for _ in content:
            yield EOSPath(path=self.path / _, mgm_url=self.mgm_url)

    def read_text(self) -> str:
        content = self._get_()
        if type(content) == list:
            return "\n".join(content)
        return ""

    def exists(self) -> bool:
        content = self._get_()
        if content is None:
            return False
        return True

    def mkdir(self) -> bool:
        parent_path = EOSPath(self.path.parent, self.mgm_url)
        parent = parent_path._get_()
        if parent == -1:
            return False  # Cannot mkdir in a file
        if parent is None:
            parent_path.mkdir()
        parent = parent_path._get_()

        name = str(self.name)
        if not name in parent:
            parent[name] = {}
        return True

    def copy(self, new_path: 'EOSPath') -> bool:
        return True  # Not implemented as not used at the moment
