from ..reader import processes
import re
from pathlib import Path
from ..reader.utils import EOSPath
from ..exceptions import ConfigError
import pytest

class TestProcesses:
    @pytest.fixture
    def test_doc(self):
        return {"executable": "test_exe", "options": "opt1 opt2 {dyn1}", "template": "na62_fw", "env": "myenv.sh"}

    @pytest.fixture
    def test_process(self):
        return processes.Process()

    def test_populate_expected_files(self, test_process):
        test_process.populate_expected_files(["*.root", "test.txt", "re:something*.txt"], ["test.txt.0", "toto.root.0", "other.root.0", "something_funny.txt.0", "test.txt.1", "toto.root.1", "other.root.1", "something_other.txt.1"])
        assert set(test_process.expected_files) == set(["test.txt", "toto.root", "other.root", re.compile(r"something.*\.txt")])

    def test_build_dyn_options(self, test_doc, test_process):
        test_process.load_fields(test_doc)
        assert test_process.build_dyn_options() == ["dyn1"]

    def test_get_args(self, test_doc, test_process):
        test_process.load_fields(test_doc)
        assert test_process.get_args() == ["inputlist", "dyn1"]

    def test_load_fields_ok(self, test_doc, test_process):
        test_process.load_fields(test_doc)
        assert(test_process.executable == Path("test_exe"))
        assert(test_process.template == "na62_fw")

    def test_load_fields_missing(self, test_doc, test_process):
        miss_doc = test_doc.copy()
        del miss_doc["executable"]
        with pytest.raises(ConfigError):
            test_process.load_fields(miss_doc)

    def test_load_fields_invalid(self, test_doc, test_process):
        with pytest.raises(ConfigError):
            test_process.load_fields({**test_doc, "invalid": True})

    def test_prepare_copy(self, test_doc, test_process):
        test_process.load_fields(test_doc)
        test_process.prepare_copy([{"file": "file1"}, {"file": "file2"}, {"directory": "dir1"}])
        assert(test_process.dir_copy[0] == (Path('dir1'), 'dir1'))
        assert(test_process.file_copy[0] == (Path('file1'), 'file1'))
        assert(test_process.file_copy[1] == (Path('file2'), 'file2'))

    def test_prepare_shell_script(self, test_doc, test_process):
        test_process.load_fields(test_doc)
        from ..reader import fw_templates as tmpl
        from ..reader import payload_templates as payloads
        output_loc=EOSPath("/eos/user/n/nlurkin/test_output_loc")
        output_what="*.root"
        unique_hash="hashval"
        arguments = tmpl.arguments.format(additional_args="INPUTLIST=$3\nDYN1=$4")
        copy_input = tmpl.local_input_copy.format()
        extract_input = tmpl.extract_inputs.format(unique_hash=unique_hash)
        payload = payloads.na62_fw[0].format(source_cmds="source myenv.sh", executable=test_process.executable, options="opt1 opt2 ${DYN1}")
        check_return = tmpl.check_return.format()
        copy_output = tmpl.eos_output_copy.format(output_what=output_what, EOS_MGM_URL=output_loc.mgm_url, output_loc=output_loc)
        final = tmpl.main_template.format(arguments=arguments, copy_input=copy_input,
                                          extract_inputs = extract_input,
                                          payload=payload, check_return = check_return,
                                          copy_output=copy_output)
        assert(test_process.prepare_shell_script(output_loc=EOSPath("/eos/user/n/nlurkin/test_output_loc"), output_what=["*.root"], unique_hash="hashval", input_system="EOS")==final)
