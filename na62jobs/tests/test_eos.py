from ..reader.eos import EOSPath
import pytest

@pytest.fixture
def my_path():
  return EOSPath("xroot://eosna62.cern.ch//eos/experiment/na62")


def test_parent(my_path):
  assert str(my_path.parent) == "/eos/experiment"

def test_name(my_path):
  assert my_path.path.name == "na62"

def test_append(my_path):
  assert str(my_path / "other") == "/eos/experiment/na62/other"


# def iterdir(self) -> Iterator['EOSPath']:
#     status, listing = self.client.dirlist(
#         str(self.path), DirListFlags.STAT)
#     if status.ok:
#         for _ in listing:
#             yield EOSPath(path=self.path / _.name, mgm_url=self.mgm_url)


    # def read_text(self) -> str:
    #     ret = ""
    #     with XRootD.client.File() as f:
    #         f.open(f"{self.mgm_url}/{self.path}", OpenFlags.READ)
    #         ret = f.read()[1].decode("utf-8")
    #     return ret

    # def exists(self) -> bool:
    #     status, _ = self.client.stat(str(self.path))
    #     if status.ok:
    #         return True
    #     return False

    # def mkdir(self) -> bool:
    #     status, _ = self.client.mkdir(str(self.path), MkDirFlags.MAKEPATH)
    #     if status.ok:
    #         return True
    #     return False

    # def rmdir(self, recurse: bool = False) -> bool:
    #     # Find out how to do it
    #     # status, _ = client.rmdir(path, recurse)
    #     # if status.ok:
    #     #   return True
    #     return False

    # def copy(self, new_path: 'EOSPath') -> bool:
    #     # Works only if MGM_URL is the same?
    #     if self.mgm_url != new_path.mgm_url:
    #         raise IOError("EOS copy works only on the same namespace")

    #     status, _ = self.client.copy(str(self.path), str(new_path), force=True)
    #     return status.ok
