merge = ("""
# Setup the environment
{source_cmds}
./{executable.name} local_input.list ${{DIRNAME}}.root
""", {"inputlist": True, "dirname": True})

na62_fw = ("""
# Setup the environment
{source_cmds}
./{executable.name} -l local_input.list {options}
""", {"inputlist": True})

na62_mc = ("""
# Setup the environment
{source_cmds}
./{executable.name} {options}
""", {"inputlist": False})