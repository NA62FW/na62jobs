import enum
import hashlib
import re
from os.path import commonprefix
from distutils.version import LooseVersion
from pathlib import Path
from typing import Union

from tqdm import tqdm

from .. import logger as log
from ..exceptions import ConfigError, ConsistencyError, SampleError
from .eos import EOSPath
from .persistency import PersistencyMixin
from .utils import is_int, make_path

eos_experiment = EOSPath("/eos/experiment/na62/data/")
eos_data = eos_experiment / "offline" / "lists" / "Data"
eos_mc = eos_experiment / "offline" / "lists" / "MC"


class InputStatus(enum.IntEnum):
    NEEDED = enum.auto()
    FAILED = enum.auto()
    COMPLETED = enum.auto()
    GONE = enum.auto()
    RUNNING = enum.auto()
    UNKNOWN = enum.auto()


class Properties(dict):
    def str_id(self):
        return ":".join([f"{_}_{self[_]}" for _ in sorted(self.keys())])


class InputRecord(object):
    def __init__(self, file: str, properties: Union[Properties, None] = None, serialized_dict: Union[dict, None] = None) -> None:
        if serialized_dict is None:
            self.input_value = file
            # Immutable once set at build (or rebuild for those that are in NEEDED state)
            self.job_id = -1
            if properties is None:
                self.properties = Properties()
            else:
                self.properties = properties
            self.status = InputStatus.NEEDED
        else:
            self.input_value = serialized_dict["input_value"]
            self.job_id = serialized_dict["job_id"]
            self.properties = Properties(serialized_dict["properties"])
            self.status = InputStatus(serialized_dict["status"])

    def set_job_id(self, job_id: int) -> None:
        # Can change only in NEEDED state.
        # In other cases, the job has already been submitted and we would lose the link
        if self.status == InputStatus.NEEDED:
            self.job_id = job_id

    def set_subdir(self, subdir: str) -> None:
        # Can change only in NEEDED state.
        # In other cases, the job has already been submitted and we would lose the link
        if self.status == InputStatus.NEEDED:
            self.properties["subdir"] = subdir

    def update_status(self, status: InputStatus) -> None:
        self.status = status

    def serialize(self) -> dict:
        return {"input_value": self.input_value,
                "job_id": self.job_id,
                "properties": self.properties,
                "status": self.status.value}

    def __str__(self) -> str:
        return self.input_value


@PersistencyMixin.create_persistency(attr_to_save=["list_inputs_groups", "src_path", "resolved", "classtype", "hash", "raw_hash"],
                                     pers_version=1,
                                     attr_transform={"src_path": make_path,
                                                     "list_inputs_groups": lambda v: {lst: [InputRecord("", serialized_dict=_) for _ in files] for lst, files in v.items()}})
class GenericInput(PersistencyMixin):
    def __init__(self) -> None:
        self.list_inputs_flat = []
        self.list_inputs_groups = {}
        self.src_path = None
        self.resolved = False
        self.classtype = "Generic"
        self.hash = ""
        self.raw_hash = ""
        self.sort_key = lambda k: Path(k.input_value).name

    def __len__(self) -> int:
        return len(self.list_inputs_flat)

    def len_lists(self) -> int:
        return len(self.list_inputs_groups)

    def load(self, data: dict, check_consistency: bool = True) -> None:
        """
        Overload the PersistencyMixin load as we have an operation to do right after
        """
        super().load(data, check_consistency)
        self.list_inputs_flat = sorted([record for lst in self.list_inputs_groups
                                        for record in self.list_inputs_groups[lst]],
                                       key=self.sort_key)

    def resolve_sample(self, update: bool=False) -> int:
        return 0

    def get_name(self) -> str:
        return ""

    def get_hash(self) -> str:
        return self.hash

    def get_raw_hash(self) -> str:
        return self.raw_hash

    def update_files_status(self, job_id_range: Union[list, None], init_status: Union[list, InputStatus], new_status: InputStatus) -> None:
        if not isinstance(init_status, list):
            init_status = [init_status]
        if job_id_range is None:
            [file.update_status(new_status)
             for file in self.list_inputs_flat if file.status in init_status]
        else:
            [file.update_status(new_status) for file in self.list_inputs_flat
             if file.job_id in job_id_range and file.status in init_status]

    def get_files(self, with_status: Union[InputStatus, None] = None, with_job_id: Union[list, None] = None) -> list:
        ret_list = []
        for file in self.list_inputs_flat:
            if with_status is not None and file.status != with_status:
                continue
            if with_job_id is not None and file.job_id not in with_job_id:
                continue
            ret_list.append(file)
        return ret_list

    def is_complete(self) -> bool:
        return len(self.get_files(with_status=InputStatus.COMPLETED)) == len(self.list_inputs_flat)

    def reset_inputs_job_id(self) -> None:
        [_.set_job_id(-1) for _ in self.list_inputs_flat]

    def get_fs_set(self) -> set:
        def fs_instance(path: str) -> str:
            if "afs" in path:
                return "afs"
            elif "eos" in path:
                if "/eos/experiment/" in path:
                    return "eos_na62"
                elif "/eos/user/" in path:
                    return "eos_user"
                else:
                    return "eos_public"
            else:
                return "local"

        used_fs = set(fs_instance(_.input_value) for _ in self.list_inputs_flat)
        return used_fs

    def check_fs(self) -> None:
        fs_set = self.get_fs_set()
        if len(fs_set) > 1:
            raise ConfigError(f"Input {self.get_name()} contains files located on multiple file systems ({', '.join(fs_set)}). This is not supported.")

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------
    def get_provides(self) -> tuple:
        return self.get_name(), self.provides_what()

    def provides_what(self) -> dict:
        return {"inputlist": True}

    # ----------------------------------------------------------------
    # Methods for Prepare step
    # ----------------------------------------------------------------
    def map(self, nfiles_per_chunk: int) -> list:
        # Files with different subdir must not be mixed in the same chunk, so let's chunk  by subdir
        all_chunks = []
        for prop, val in self.get_all_properties():
            # Now split in chunks
            files_to_chunk = [_ for _ in self.list_inputs_flat if _.job_id == -1 and _.properties.str_id() == prop]
            chunks = [files_to_chunk[i:i + nfiles_per_chunk]
                    for i in range(0, len(files_to_chunk), nfiles_per_chunk)]
            all_chunks.extend(chunks)
        return all_chunks

    def split_runs(self) -> bool:
        return False

    # ----------------------------------------------------------------
    # Methods for Submit step
    # ----------------------------------------------------------------
    def get_needed_chunks(self) -> list:
        needed_files = self.get_files(with_status=InputStatus.NEEDED)
        return [(k,v) for k,v in {_.job_id: _.properties for _ in needed_files}.items()]

    # ----------------------------------------------------------------
    # Methods for check step
    # ----------------------------------------------------------------
    def get_all_properties(self) -> list:
        return [(k,v) for k,v in {_.properties.str_id(): _.properties for _ in self.list_inputs_flat}.items()]


@PersistencyMixin.create_persistency(attr_to_save=["version", "found_version", "filter"],
                                     pers_version=1,
                                     attr_transform={})
class OfficialInput(GenericInput):
    def __init__(self, input_dict) -> None:
        super().__init__()
        self.version = "latest"
        self.found_version = None
        self.filter = None
        if "version" in input_dict:
            self.version = input_dict["version"]
        if "filter" in input_dict:
            self.filter = input_dict["filter"]

    def summary(self, name: str) -> str:
        if self.resolved:
            return (f" - Sample: {self.classtype:>10}({name:<10}) Version : {self.version} -> {str(self.found_version):>8}\n"
                    f"{' ':>22}Filter  : {self.filter}\n"
                    f"{' ':>22}#Lists  : {self.len_lists()}\n"
                    f"{' ':>22}#Files  : {len(self)}\n"
                    f"{' ':>22}Found at: {self.src_path}"
                    )
        else:
            return f"Requested sample {self.classtype:>10}({name:<10}) - Version: {self.version} - Filter: {self.filter}"

    def check_consistency(self, data: dict, check_consistency: bool = True) -> None:
        super().check_consistency(data, check_consistency)
        if check_consistency and (self.filter != data["filter"]):
            raise ConsistencyError(
                f"Filter {data['filter']} does not match filter {self.filter}")

    def set_hash(self, add_name: str, raw: bool = False) -> None:
        if raw:
            self.raw_hash = hashlib.md5(
                f"{self.version}:{self.filter}:{add_name}".encode("utf8")).hexdigest()
        else:
            self.hash = hashlib.md5(
                f"{self.found_version}:{self.filter}:{add_name}".encode("utf8")).hexdigest()

    def check_versions(self, year: str, period: str) -> str:
        src_path = eos_data / year
        samples_in_year = [_.name for _ in src_path.iterdir()]
        valid_samples = [s for s in samples_in_year if period in s]
        versions = set([s.split('-')[-1] for s in valid_samples])
        found_version = None
        if not self.version.startswith("latest") and all(not v.startswith(self.version) for v in versions):
            raise SampleError(
                f"Version {self.version} not found for sample {period}")
        elif len(versions) == 0:
            raise SampleError(
                f"No version found for sample {period}")
        elif self.version.startswith("latest"):
            if self.version == 'latest-common' and self.found_version is not None and self.found_version not in versions:
                raise SampleError(
                    f"No common version number found between the requested run numbers")
            found_version = sorted(versions, key=LooseVersion)[-1]
        else:
            found_version = sorted(filter(lambda v: v.startswith(self.version), versions), key=LooseVersion)[-1]
        return found_version

    def get_lists(self, year: str, period: str, version: str, runs: Union[list, None]) -> tuple:
        src_path = eos_data / year / f"{period}-{version}"
        all_lists = [_.name for _ in src_path.iterdir(check_is_dir=True)]
        avail_filters = set()
        valid_lists = []
        for lst in all_lists:
            m = re.search(r"Run([0-9]+)\.([a-zA-Z0-9]+)\..*", lst)
            if m is not None:
                if runs is not None and int(m.group(1)) not in runs:
                    continue
                avail_filters.add(m.group(2))
                valid_lists.append(lst)
        if self.filter not in avail_filters:
            raise SampleError(
                f"Filter {self.filter} not found for sample {period}")
        valid_lists = [s for s in valid_lists if re.search(r"Run([0-9]+)\." + self.filter + "\.", s)]
        return valid_lists, src_path

    def extract_files_from_lists(self, valid_lists: list, period: str, src_path: Union[Path, EOSPath]) -> None:
        for lst in tqdm(valid_lists, leave=False, desc=f"Extracting ROOT files in sample {period}"):
            if self.filter is None or self.filter in lst:
                lines_in_file = [l.strip() for l in (src_path / lst).read_text().split("\n")]
                root_files = [l for l in lines_in_file if len(l) > 0 and l[0] != '#']
                self.list_inputs_groups[lst] = [InputRecord(_) for _ in root_files]
                self.list_inputs_flat.extend(self.list_inputs_groups[lst])

    def update_files_from_lists(self, valid_lists: list, period: str, src_path: Union[Path, EOSPath]) -> None:
        existing_files = set(_.input_value for _ in self.list_inputs_flat)
        new_files = 0
        for lst in tqdm(valid_lists, leave=False, desc=f"Extracting ROOT files in sample {period}"):
            if self.filter is None or self.filter in lst:
                lines_in_file = [l.strip() for l in (src_path / lst).read_text().split("\n")]
                root_files = [l for l in lines_in_file if len(l) > 0 and l[0] != '#']

                # Remove files that are already present
                root_files = list(set(root_files) - existing_files)
                updated = [InputRecord(_) for _ in root_files]
                new_files += len(updated)
                self.list_inputs_groups[lst].extend(updated)
                self.list_inputs_flat.extend(updated)
        log.item(f"{new_files} new files were found and have been added")

    def split_runs(self) -> bool:
        for lst in self.list_inputs_groups:
            run_number = re.search(r"Run([0-9]{6})", lst)
            if not run_number:
                raise ValueError(f"Unable to determine run number for list file {lst}")
            run_number = int(run_number.group(1))
            subdir = f"run{run_number}"
            [f.set_subdir(subdir) for f in self.list_inputs_groups[lst]]

        return True


@PersistencyMixin.create_persistency(attr_to_save = ["sample_name"],
                                     pers_version = 1,
                                     attr_transform = {})
class DataSampleInput(OfficialInput):
    def __init__(self, sample: dict) -> None:
        super().__init__(sample)
        self.sample_name = sample["name"]
        self.classtype = "DataSample"
        self.set_hash(self.sample_name, raw=True)

    def summary(self) -> str:
        return super().summary(self.get_name())

    def get_name(self) -> str:
        return self.sample_name

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------
    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.sample_name}")
        if self.filter is None:
            raise ConfigError(
                f"'filter' field is required for data sample {self.sample_name}")
        year = self.sample_name[0:4]
        self.found_version = self.check_versions(year, self.sample_name)
        valid_lists, self.src_path = self.get_lists(
            year, self.sample_name, self.found_version, None)

        if update:
            self.update_files_from_lists(valid_lists, self.sample_name, self.src_path)
        else:
            self.extract_files_from_lists(valid_lists, self.sample_name, self.src_path)

        if not update:
            self.resolved = True
            self.list_inputs_flat = sorted(
                self.list_inputs_flat, key=lambda k: Path(k.input_value).name)

            self.set_hash(self.sample_name)

        self.check_fs()
        return len(self)


@PersistencyMixin.create_persistency(attr_to_save=["sample_name", "mc_type", "reco_version", "run_number", "found_reco_version"],
                                     pers_version=1,
                                     attr_transform={})
class MCSampleInput(OfficialInput):
    allowed_mc_types = ["mc", "mc_noov", "mcdump", "mc_biasedovk3pi", "mc_biasedov", "mc_biasedovk2piD", "mc_biasedovk3pihalo",
                        "mc_biasedovk3pikmu2", "fastmc", "mc_biasedovk3pi0d", "mc_biasedovke3d", "mc_biasedovke4", "mc_biasedovkmu3d"]

    def __init__(self, sample: dict) -> None:
        super().__init__(sample)
        self.sample_name = sample["name"]
        self.classtype = "MCSample"
        self.set_hash(self.sample_name, raw=True)
        self.mc_type = None
        if "type" in sample:
            self.mc_type = sample["type"]
        self.run_number = None
        if "run_number" in sample:
            self.run_number = sample["run_number"]
        self.reco_version = "latest"
        if "reco_version" in sample:
            self.reco_version = sample["reco_version"]
        self.found_reco_version = None

    def summary(self) -> str:
        if self.resolved:
            return (f" - Sample: {self.classtype:>10}({self.get_name():<10}) Version : {self.version} -> {str(self.found_version):>8}\n"
                    f"{' ':>22}Reco    : {self.reco_version} -> {str(self.found_reco_version):>8}\n"
                    f"{' ':>22}Type    : {self.mc_type}\n"
                    f"{' ':>22}Run     : {self.run_number}\n"
                    f"{' ':>22}#Lists  : {self.len_lists()}\n"
                    f"{' ':>22}#Files  : {len(self)}\n"
                    f"{' ':>22}Found at: {self.src_path / list(self.list_inputs_groups.keys())[0]}"
                    )
        else:
            return f"Requested sample {self.classtype:>10}({self.get_name():<10}) - Version: {self.version} - Type: {self.mc_type} - Run: {self.run_number} - Reco version: {self.reco_version}"


    def check_consistency(self, data: dict, check_consistency: bool = True) -> None:
        super().check_consistency(data, check_consistency)
        # Add consistency check here fo mc_type
        if check_consistency:
            if self.mc_type != data["mc_type"]:
                raise ConsistencyError("Inconsistent mc_type")
            if self.reco_version != data["reco_version"]:
                raise ConsistencyError("Inconsistent reco_version")
            if self.run_number != data["run_number"]:
                raise ConsistencyError("Inconsistent run_number")


    def get_name(self) -> str:
        return self.sample_name

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------
    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.sample_name}")
        if self.mc_type is None:
            raise ConfigError(
                f"'type' field is required for mc sample {self.sample_name}")
        if self.run_number is None:
            raise ConfigError(
                f"'run_number' field is required for mc sample {self.sample_name}")
        if self.mc_type not in MCSampleInput.allowed_mc_types:
            raise ConfigError(
                f"'type' field is expected to be either of {MCSampleInput.allowed_mc_types} for mc sample {self.sample_name}")

        version_type = {}
        re_search = re.compile(
            r"(.+)\.Run([0-9]+)\.(?:RECO|RECODUMP).(" + "|".join(MCSampleInput.allowed_mc_types) + r")\..*")
        # Find lists where sample is found
        for rev in eos_mc.iterdir():
            rev_name = rev.name
            if re.match(r"v[0-9]+\.[0-9]+\.[0-9]+", rev_name):
                for rev_rev in rev.iterdir():
                    rev_rev_name = rev_rev.name
                    if rev_rev_name == "Reco_noov":
                        # grid reco -> not supported
                        continue
                    for sample in rev_rev.iterdir():
                        sample_file_name = sample.name
                        m = re_search.match(sample_file_name)
                        if m is None:
                            print("Error", rev_name, rev_rev_name, sample_file_name)
                            continue
                        sample_name = m.group(1)
                        run_number = int(m.group(2))
                        mc_type = m.group(3)
                        if sample_name == self.sample_name and run_number == self.run_number and mc_type == self.mc_type:
                            reco_rev = rev_rev_name.split("-")[1]
                            if rev_name not in version_type:
                                version_type[rev_name] = {}
                            if reco_rev not in version_type[rev_name]:
                                version_type[rev_name][reco_rev] = []
                            version_type[rev_name][reco_rev].append(str(sample))

        if len(version_type)==0:
            raise ConfigError(f"No list could be found for the input sample {self.sample_name}.")

        if self.version == "latest":
            self.found_version = sorted(
                version_type.keys(), key=LooseVersion)[-1]
        elif self.version not in version_type.keys():
            raise SampleError(
                f"Version {self.version} not found for sample {self.sample_name}")
        else:
            self.found_version = self.version
        used_version = version_type[self.found_version]
        if self.reco_version == "latest":
            self.found_reco_version = sorted(
                used_version.keys(), key=LooseVersion)[-1]
        elif self.reco_version not in used_version.keys():
            raise SampleError(
                f"Reco version {self.reco_version} not found for sample {self.sample_name}")
        else:
            self.found_reco_version = self.reco_version
        used_lists = used_version[self.found_reco_version]

        if len(used_lists) != 1:
            raise SampleError(
                f"Multiple valid lists found for {self.sample_name}")
        self.src_path = make_path(used_lists[0]).parent
        if update:
            self.update_files_from_lists(used_lists, self.sample_name, self.src_path)
        else:
            self.extract_files_from_lists(used_lists, self.sample_name, self.src_path)

        if not update:
            self.resolved = True
            self.list_inputs_flat = sorted(self.list_inputs_flat,
                                    key=lambda k: Path(k.input_value).name)

            self.set_hash(self.sample_name)

        self.check_fs()
        return len(self)


@PersistencyMixin.create_persistency(attr_to_save=["run_numbers", "sample_name"],
                                     pers_version=1,
                                     attr_transform={})
class RunsInput(OfficialInput):
    runs_cache = {}
    def __init__(self, runs: dict) -> None:
        super().__init__(runs)
        self.run_numbers = runs["numbers"]
        self.classtype = "Runs"
        self.set_hash(self.run_numbers, raw=True)
        if "name" in runs:
            self.sample_name = runs["name"]
        else:
            self.sample_name = self.get_raw_hash()[:7]

    def summary(self) -> str:
        return super().summary(self.get_name())

    def check_consistency(self, data: dict, check_consistency: bool = True) -> None:
        super().check_consistency(data, check_consistency)

        if check_consistency:
            if self.run_numbers != data["run_numbers"]:
                raise ConsistencyError("Inconsistent run_numbers")
            if self.sample_name != data["sample_name"]:
                raise ConsistencyError("Inconsistent sample_name")


    def get_name(self) -> str:
        return str(self.sample_name)

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------
    def build_cache(self) -> dict:
        if len(RunsInput.runs_cache) == 0:
            years = [_.name for _ in eos_data.iterdir() if is_int(_.name)]
            samples = set()
            samples_versions = {}
            for year in years:
                samples_in_year = [_.name for _ in (eos_data/year).iterdir()]
                samples_set = set(
                    (year, s.split("-")[0]) for s in samples_in_year if s.split("-")[0] != year)
                samples = samples.union(samples_set)
                for sample in samples_set:
                    samples_versions[sample[1]] = tuple(
                        _ for _ in samples_in_year if sample[1] in _)

            runs_for_sample = {}
            with tqdm(sorted(samples), leave=False) as t:
                for year, sample in t:
                    t.set_description(desc=f"Building run cache for {sample}")
                    runs = set()
                    for sample_w_version in samples_versions[sample]:
                        lists_in_sample = [_.name for _ in (eos_data/year/sample_w_version).iterdir()]
                        for lst in lists_in_sample:
                            m = re.search(r"Run([0-9]+)\.([a-zA-Z0-9]+)\..*", lst)
                            if m is not None:
                                runs.add(int(m.group(1)))
                    runs_for_sample[(year, sample)] = runs
            RunsInput.runs_cache = runs_for_sample

        return RunsInput.runs_cache

    def list_periods(self, run_numbers) -> set:
        runs_for_sample = self.build_cache()
        samples_in_use = set()
        for run in run_numbers:
            samples_w_run = [
                _ for _ in runs_for_sample if run in runs_for_sample[_]]
            if len(samples_w_run) == 0:
                raise ConfigError(
                    f"Run number {run} was not found in any run period")
            if len(samples_w_run) > 1:
                raise ConfigError(
                    f"Run number {run} was found in multiple periods")
            samples_in_use.add(samples_w_run[0])
        return samples_in_use

    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.get_name()}")
        if self.filter is None:
            raise ConfigError(
                f"'filter' field is required for samples of type \"runs\"")

        all_periods = self.list_periods(self.run_numbers)
        self.src_path = eos_data
        for year, period in all_periods:
            found_version = self.check_versions(year, period)
            if self.found_version is None:
                self.found_version = found_version
            elif found_version != self.found_version:
                self.found_version = commonprefix([self.found_version, found_version]) + "*"
            valid_lists, src_path = self.get_lists(
                year, period, found_version, self.run_numbers)
            if update:
                self.update_files_from_lists(valid_lists, period, src_path)
            else:
                self.extract_files_from_lists(valid_lists, period, src_path)

        if not update:
            self.resolved = True
            self.list_inputs_flat = sorted(
                self.list_inputs_flat, key=lambda k: Path(k.input_value).name)

            self.set_hash(str(self.run_numbers))

        self.check_fs()
        return len(self)


@PersistencyMixin.create_persistency(attr_to_save=[],
                                     pers_version=1,
                                     attr_transform={})
class ListInput(GenericInput):
    def __init__(self, list_path: str) -> None:
        super().__init__()
        self.classtype = "List"
        self.src_path = Path(list_path).resolve()
        self.raw_hash = hashlib.md5(list_path.encode("utf8")).hexdigest()

    def summary(self):
        if self.resolved:
            return (f" - Sample: {self.classtype:>10}({self.get_name()})\n"
                    f"{' ':>22}#Files  : {len(self)}\n"
                    f"{' ':>22}Found at: {self.src_path}"
                    )
        else:
            return f"Requested sample {self.classtype}({self.get_name()})"

    def get_name(self) -> str:
        return self.src_path.name

    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.get_name()}")
        if not self.src_path.exists():
            raise ConfigError(f"List file {self.src_path} does not exist")

        root_files = [l.strip() for l in self.src_path.read_text().split("\n")]
        root_files = [l for l in root_files if len(l) > 0 and l[0] != '#']

        list_key = str(self.src_path)
        if update:
            existing_files = set(_.input_value for _ in self.list_inputs_flat)
            root_files = list(set(root_files) - existing_files)
            updated = [InputRecord(_) for _ in root_files]
            log.item(f"{len(updated)} new files were found and have been added")
            self.list_inputs_groups[list_key].extend(updated)
            self.list_inputs_flat.extend(updated)
        else:
            self.list_inputs_groups[list_key] = [InputRecord(_) for _ in root_files]
            self.list_inputs_flat.extend(self.list_inputs_groups[list_key])
            self.resolved = True
            self.list_inputs_flat = sorted(self.list_inputs_flat,
                                    key=lambda k: Path(k.input_value).name)

            self.hash = hashlib.md5(str(self.src_path).encode("utf8")).hexdigest()

        self.check_fs()
        return len(self)


@PersistencyMixin.create_persistency(attr_to_save=["subdirs"],
                                     pers_version=1,
                                     attr_transform={})
class MergeInput(GenericInput):
    def __init__(self, inputs: dict) -> None:
        super().__init__()
        self.classtype = "Merge"
        self.src_path = make_path(inputs["path"])
        self.subdirs = inputs["subdirs"]
        self.raw_hash = hashlib.md5(str(self.src_path).encode("utf8")).hexdigest()

    def summary(self):
        if self.resolved:
            return (f" - Sample: {self.classtype:>10}({self.get_name()})\n"
                    f"{' ':>22}#Files  : {len(self)}\n"
                    f"{' ':>22}Found at: {self.src_path}"
                    )
        else:
            return f"Requested sample {self.classtype}({self.get_name()})"

    def get_name(self) -> str:
        return self.src_path.name + "_merged"

    def provides_what(self):
        return {"inputlist": True, "dirname": True}

    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.get_name()}")
        if not self.src_path.exists():
            raise ConfigError(f"Input directory {self.src_path} does not exist")

        for subdir in self.subdirs:
            subpath = self.src_path / subdir
            root_files = []
            if subpath.exists():
                root_files = [str(l) for l in subpath.iterdir()]

            list_key = str(subpath)
            # Do something about rebuild here? Does it makes sense for merging? How?
            if update:
                raise SampleError("'Merge' input does not support rebuild")
                # existing_files = set(_.input_file for _ in self.list_inputs_flat)
                # root_files = list(set(root_files) - existing_files)
                # updated = [InputRecord(_, "") for _ in root_files]
                # log.item(f"{len(updated)} new files were found and have been added")
                # self.list_inputs_groups[list_key].extend(updated)
                # self.list_inputs_flat.extend(updated)
            else:
                self.list_inputs_groups[list_key] = [InputRecord(_, Properties({"subdir": str(subdir), "dirname": str(subpath.name)})) for _ in root_files]
                self.list_inputs_flat.extend(self.list_inputs_groups[list_key])
                self.resolved = True
                self.list_inputs_flat = sorted(self.list_inputs_flat,
                                        key=lambda k: Path(k.input_value).name)

                self.hash = hashlib.md5(str(self.src_path).encode("utf8")).hexdigest()

        self.check_fs()
        return len(self)

    def map(self, nfiles_per_chunk: int) -> list:
        # Here we need to do one chunk per list and ignore the rest
        #  -> Maybe do not ignore nfiles_per_chunk... See later (in case of really huge merge)

        return [self.list_inputs_groups[lst] for lst in self.list_inputs_groups if len(self.list_inputs_groups[lst])>0]

@PersistencyMixin.create_persistency(attr_to_save=["first", "last", "step"],
                                     pers_version=1,
                                     attr_transform={})
class RangeInput(GenericInput):
    def __init__(self, val):
        super().__init__()
        self.classtype = "Range"
        self.src_path = ""
        self.sort_key = lambda k: int(k.properties["value"])
        self.first = 0
        self.last = 0
        self.step = 1
        if len(val) == 1:
            # Meaning 0 to val with step 1
            self.last = int(val[0])
        elif len(val) == 2:
            # Meaning val[0] to val[1] with step 1
            self.first = int(val[0])
            self.last = int(val[1])
        elif len(val) == 3:
            # Meaning val[0] to val[1] with step val[2]
            self.first = int(val[0])
            self.last = int(val[1])
            self.step = int(val[2])
        else:
            raise ConfigError(f"Invalid range input: {val}. Expect 1, 2, or 3 values.")

        if self.last < self.first:
            # Ensure step is negative
            self.step = -abs(self.step)


    def provides_what(self):
        return {"inputlist": False, "value": True}

    def resolve_sample(self, update: bool=False) -> int:
        log.item(f"Solving configuration for input {self.get_name()}")

        values = list(str(_) for _ in range(self.first, self.last+self.step, self.step))

        list_key = "range"
        if update:
            existing_values = set(_.properties["value"] for _ in self.list_inputs_flat)
            values = list(set(values) - existing_values)
            updated = [InputRecord("", Properties({"value": _})) for _ in values]
            log.item(f"{len(updated)} new inputs were found and have been added")
            self.list_inputs_groups[list_key].extend(updated)
            self.list_inputs_flat.extend(updated)
        else:
            self.list_inputs_groups[list_key] = [InputRecord("", Properties({"value": _})) for _ in values]
            self.list_inputs_flat.extend(self.list_inputs_groups[list_key])
            self.resolved = True

            self.hash = hashlib.md5(str(self.src_path).encode("utf8")).hexdigest()
        return len(self)


    def get_name(self) -> str:
        return f"{self.first}_{self.last}_{self.step}"

    def summary(self) -> str:
        if self.resolved:
            return (f" - Sample: {self.classtype:>10}({self.get_name():<10}) - {self.first} -> {self.last}, step={self.step}\n"
                    f"{' ':>22}#Inputs  : {len(self)}\n")
        else:
            return f"Requested sample {self.classtype:>10}({self.get_name():<10}) - {self.first} -> {self.last}, step={self.step}"


def data_or_mc(sample_name: str) -> bool:
    # Find out if this is data or MC
    # it is data if the sample_name starts with a year (4 digits)
    if re.match(r"^[0-9]{4}", sample_name):
        return True
    return False


def build_input(input_type: str, input_dict: Union[dict, str]) -> GenericInput:
    if isinstance(input_dict, dict):
        if input_type == "samples":
            if data_or_mc(input_dict["name"]):
                return DataSampleInput(input_dict)
            else:
                return MCSampleInput(input_dict)
        elif input_type == "runs":
            return RunsInput(input_dict)
        elif input_type == "merge":
            return MergeInput(input_dict)
    else:
        if input_type == "lists":
            return ListInput(input_dict)
        elif input_type == "ranges":
            return RangeInput(input_dict)
    raise ConfigError(f"Invalid input type: {input_type}")


def load_input(input_dict: dict, check_consistency: bool) -> GenericInput:
    if input_dict["classtype"] == "DataSample":
        my_input = DataSampleInput({"name": input_dict['sample_name']})
    elif input_dict["classtype"] == "MCSample":
        my_input = MCSampleInput({"name": input_dict['sample_name']})
    elif input_dict["classtype"] == "Runs":
        my_input = RunsInput({"numbers": input_dict['run_numbers']})
    elif input_dict["classtype"] == "List":
        my_input = ListInput(input_dict['src_path'])
    elif input_dict["classtype"] == "Merge":
        my_input = MergeInput({"path": "", "subdirs": []})
    elif input_dict["classtype"] == "Range":
        my_input = RangeInput([input_dict["first"], input_dict["last"], input_dict["step"]])
    else:
        raise ValueError(
            f"Invalid input class type: {input_dict['classtype']}")
    my_input.load(input_dict, check_consistency)
    return my_input
