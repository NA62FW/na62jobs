import XRootD
from XRootD.client.flags import DirListFlags, OpenFlags, MkDirFlags
from typing import Union, Iterator
import re
from pathlib import PosixPath, Path

re_eos = re.compile(r"((?:x|)root://\w+\.\w+\.\w+)/(/[\S/-]+)")


class EOSPath(object):
    client_pool = {}

    @staticmethod
    def _get_client(mgm_url: str) -> XRootD.client:
        if mgm_url not in EOSPath.client_pool:
            EOSPath.client_pool[mgm_url] = XRootD.client.FileSystem(mgm_url)
        return EOSPath.client_pool[mgm_url]

    def __init__(self, path: Union[str, Path], mgm_url: Union[str, None] = None):
        if mgm_url is None:
            str_path = str(path)
            m = re_eos.match(str_path)
            if not m:
                # No MGM_URL provided, and not present in path
                # Infer from path
                if str_path.startswith("/eos/home-"):
                    raise ValueError(
                        f"{path} has unsupported format; Suppoted EOS paths must start with /eos/user or /eos/experiment."
                    )
                if str_path.startswith("/eos/user/"):
                    self.mgm_url = "xroot://eosuser.cern.ch"
                elif str_path.startswith("/eos/experiment/na62/"):
                    self.mgm_url = "xroot://eosna62.cern.ch"
                else:
                    raise ValueError(
                        f"{path} missing MGM_URL and cannot be inferred; must be passed as arguments, present in the path, or path must be in /eos/user/ or /eos/experiment/na62")
                self.path = PosixPath(path)
            else:
                self.mgm_url = m.group(1)
                self.path = PosixPath(m.group(2))
        else:
            self.mgm_url = mgm_url
            self.path = PosixPath(path)

        self.client = EOSPath._get_client(self.mgm_url)

    def __str__(self):
        return str(self.path)

    def full_str(self):
        return f"{self.mgm_url}/{self.path}"

    @property
    def parent(self):
        return EOSPath(self.path.parent, mgm_url=self.mgm_url)

    @property
    def name(self):
        return self.path.name

    def __truediv__(self, other: str) -> 'EOSPath':
        return EOSPath(self.path / other, mgm_url=self.mgm_url)

    def iterdir(self, check_is_dir=False) -> Iterator['EOSPath']:
        status, listing = self.client.dirlist(
            str(self.path), DirListFlags.STAT)
        if status.ok:
            for _ in listing:
                if check_is_dir:
                    flags = _.statinfo.flags
                    second_bit = (flags >> 1) & 1
                    if second_bit:
                        continue
                yield EOSPath(path=self.path / _.name, mgm_url=self.mgm_url)

    def read_text(self) -> str:
        ret = ""
        with XRootD.client.File() as f:
            f.open(f"{self.mgm_url}/{self.path}", OpenFlags.READ)
            ret = f.read()[1].decode("utf-8")
        return ret

    def exists(self) -> bool:
        status, _ = self.client.stat(str(self.path))
        if status.ok:
            return True
        return False

    def mkdir(self) -> bool:
        status, _ = self.client.mkdir(str(self.path), MkDirFlags.MAKEPATH)
        if status.ok:
            return True
        return False

    def rmdir(self, recurse: bool = False) -> bool:
        # Find out how to do it
        # status, _ = client.rmdir(path, recurse)
        # if status.ok:
        #   return True
        return False

    def copy(self, new_path: 'EOSPath') -> bool:
        # Works only if MGM_URL is the same?
        if self.mgm_url != new_path.mgm_url:
            raise IOError("EOS copy works only on the same namespace")

        status, _ = self.client.copy(str(self.path), str(new_path), force=True)
        return status.ok
