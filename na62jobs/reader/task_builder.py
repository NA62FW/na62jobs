import itertools
import json
import math
import re
import shutil
from pathlib import Path
from typing import Tuple, Union

import yaml
from tqdm import tqdm

from .. import logger as log
from ..exceptions import ConfigError, ConsistencyError, StepError
from ..htcondor.condor_submitter import BatchSubmitter, CondorSubmitter
from . import inputClasses
from .persistency import PersistencyMixin, PredefinedTransforms
from .processes import Process
from .utils import find_root_file_merger, get_FS, make_path, rmdir

required_keys = set(["task", "process", "input", "output", "temporary"])

def load_process(v: Union[None, dict]) -> Union[None, Process]:
    if v is None:
        return None
    p = Process()
    p.load(v, False)
    return p

def monotonic_list_to_range(lst):
    out = []
    for _, g in itertools.groupby(enumerate(lst), lambda k: k[0] - k[1]):
        start = next(g)[1]
        end = list(v for _, v in g) or [start]
        out.append((start, end[-1]))
    return out


@PersistencyMixin.create_persistency(attr_to_save=["name", "output_loc", "output_what", "nfiles_per_job",
                                                   "njobs", "total_chunks", "cluster_id", "can_modify",
                                                   "input", "process", "merge_input", "merge_process"],
                                     pers_version=1,
                                     attr_transform={"output_loc": make_path,
                                                     "merge_input": lambda v: None if v is None else inputClasses.load_input(v, False),
                                                     "merge_process": load_process,
                                                     "input": PredefinedTransforms.DEFER,
                                                     "process": PredefinedTransforms.DEFER
                                                     })
class Task(PersistencyMixin):
    merge_output_what = ["re:*.root"]

    def __init__(self, document: Union[dict, None] = None, save_file: Union[Path, None] = None) -> None:
        if document is not None:
            self.name = document["task"]
            self.job_directory = Path(document["temporary"]["job_dir"])
        elif save_file is not None:
            self.name = save_file.stem
            self.job_directory = save_file.parent.parent # since the json is always created in /sonme/path/task_name/file.json
        else:
            raise ConfigError(
                "Task requires either a document, or a json save_file")

        self.input = []
        self.output_loc = Path()
        self.output_what = []
        self.process = Process()
        self.nfiles_per_job = -1
        self.njobs = -1
        self.job_submitter = None
        self.total_chunks = 0
        self.cluster_id = []
        self.merge_input = None
        self.merge_process = None
        self.can_modify = True

    def summary(self, print_processing: bool = True) -> None:
        log.title(f"{self.name}")
        log.item(f"Job directory: {self.get_job_directory()}")
        log.item("Inputs:")
        njobs_string = "Number of jobs: "
        if self.njobs > 0:
            njobs_string += f"{self.njobs} jobs"
        if self.nfiles_per_job > 0:
            if self.njobs > 0:
                njobs_string += f" ({self.nfiles_per_job} files/job)"
            else:
                njobs_string += f"{self.nfiles_per_job} files/job"
        log.subitem(njobs_string)
        for input in self.input:
            log.subitem(input.summary())
        log.item(
            f"Output dir: {self.output_loc} (on FS {get_FS(self.output_loc)})")
        resolved_expected = ""
        if self.process.expected_files:
            resolved_expected = "[" + ", ".join(sorted(str(_) for _ in self.process.expected_files)) + "]"
        log.item(f"Output files: {', '.join(self.output_what)} {resolved_expected}")
        log.item(self.process.summary())

        if print_processing and self.njobs > 0:
            self.summary_processing()

    def display_input_status(self, input) -> None:
        self.std_bar("Completed", total=len(input),
                        n=len(input.get_files(
                            with_status=inputClasses.InputStatus.COMPLETED)),
                        color="green")
        if not input.is_complete():
            self.std_bar("Running", total=len(input),
                            n=len(input.get_files(
                                with_status=inputClasses.InputStatus.RUNNING)),
                            color="blue")
            self.std_bar("Failed", total=len(input),
                            n=len(input.get_files(
                                with_status=inputClasses.InputStatus.FAILED)),
                            color="red")
        unknown_files = input.get_files(with_status=inputClasses.InputStatus.UNKNOWN)
        if len(unknown_files)>0:
            self.std_bar("Unknown", total=len(input),
                            n=len(unknown_files),
                            color="white")

    def summary_processing(self) -> None:
        if len(self.cluster_id) != 0:
            # Submission ongoing. Print current status
            log.step(f"Jobs last submitted on batches #{', '.join(str(_[0]) for _ in self.cluster_id)}")
            log.title(
                f"Number of jobs in current submissions: {sum(len(_[1]) for _ in self.cluster_id)}/{self.total_chunks}")
        else:
            log.step(f"No submission currently ongoing")

        log.title("Processing status")

        for input in self.input:
            log.item(f"Sample {input.get_name()}")
            self.display_input_status(input)

        if self.merge_input is not None:
            log.title("Merging status")
            self.display_input_status(self.merge_input)

    def detail_failed(self) -> None:
        log.step("Printing all jobs with failed status")
        for input in self.input:
            log.title(f"Sample {input.get_name()}")
            jobs = set([_.job_id for _ in input.get_files(with_status=inputClasses.InputStatus.FAILED)])
            job_ranges = [f"{s}" if s==e else f"{s} -> {e}" for s,e in monotonic_list_to_range(sorted(jobs))]
            log.item(f"Jobs numbers in ranges: " + ", ".join(job_ranges))

    def get_submitter(self) -> BatchSubmitter:
        if self.job_submitter is None:
            self.job_submitter = CondorSubmitter()
        return self.job_submitter

    def std_bar(self, label: str,  n: int, total: int, color: str) -> None:
        ndigits = str(len(str(total)))
        t = tqdm(total=total, initial=n, unit="files", colour=color, leave=True,
                 bar_format="{l_bar}{bar:30}| {n_fmt:" +
                 ndigits + "}/{total_fmt} {unit}",
                 desc=f"{'-':>8}{label:>12}")
        t.close()

    def get_job_directory(self) -> Path:
        return self.job_directory / self.name

    ### Saving and loading ###
    def load(self, config: dict, check_consistency: bool = True) -> None:
        super().load(config, check_consistency)

        # Now we deal with the complex input and process fields
        input_dict = {_["raw_hash"]: _ for _ in config["input"]}
        if len(self.input) == 0 or not check_consistency:
            # Loading from cache necessarily = either loaded with --job_dir, or with config but not the rebuild command
            self.input = []
            for input in input_dict:
                self.input.append(inputClasses.load_input(input_dict[input], check_consistency))
        else:
            # Updating from config - with the rebuild command
            for input in self.input:
                if input.get_raw_hash() in input_dict:
                    input.load(input_dict[input.get_raw_hash()], check_consistency)
        self.process.load(config["process"], check_consistency)

    def check_consistency(self, config: dict, check_consistency: bool = True) -> None:
        if check_consistency:
            if str(self.output_loc) != str(make_path(config["output_loc"])):
                raise ConsistencyError("Inconsistent output_loc")
            if self.output_what != config["output_what"]:
                raise ConsistencyError("Inconsistent output_what")


    def load_cache(self, check_consistency: bool = True) -> None:
        print()
        log.step(f"Loading solved configuration request for task {self.name}")
        json_file = self.get_job_directory() / f"{self.name}.json"
        if not json_file.exists():
            raise ConfigError(
                f"Job directory {json_file} does not exist - Cannot load cache")

        with open(json_file, "r") as f:
            config = json.load(f)
        self.load(config, check_consistency)

    def compute_njobs(self, njobs: int, nfiles_per_job: int, ninput_files: int) -> Tuple[int, int]:
        if njobs > 0:
            nfiles_per_job = math.ceil(ninput_files / njobs)
        elif nfiles_per_job > 0:
            njobs = math.ceil(ninput_files / nfiles_per_job)

        return njobs, nfiles_per_job

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------

    ### Processing sections of the config file ###
    # - This only extract the information from the configuration file
    # and fills the Task object information.
    # Checks are done to verify that the configuration is complete
    # - Five sections: task, process, input, output, temporary
    # Although temporary is trivial and done in the constructor
    def handle_input_section(self, input_field: dict) -> None:
        for input in input_field:
            if input == "njobs":
                self.njobs = input_field[input]
            elif input == "nfiles_per_job":
                self.nfiles_per_job = input_field[input]
            else:
                for entry in input_field[input]:
                    input_instance = inputClasses.build_input(input, entry)
                    if input_instance is not None:
                        self.input.append(input_instance)
                    else:
                        ConfigError(f"Uknown input class type {input}")
        if self.njobs != -1 and self.nfiles_per_job != -1:
            raise ConfigError(
                f"njobs and nfiles_per_job are mutually exclusive")

    def handle_output_section(self, output_field: dict) -> None:
        required = ("location", "what")
        if not all(_ in output_field for _ in required):
            raise ConfigError(f"Missing required keys: {required}")

        # Deal with location part of the output
        location = output_field["location"]
        if not isinstance(location, str):
            raise ConfigError(f"Expect a string for output:location")
        self.output_loc = make_path(location)

        # Deal with list of output files
        self.output_what = output_field["what"]

    def handle_process_section(self, process_field: dict) -> None:
        self.process.load_fields(process_field)

    ### Solving configuration ###
    # - The configuration options are used and chacked that a viable
    # task can be run. Checks are done to verify that all the input/output
    # are found. We actually act on the variables to create the correct
    # file structure in the system.
    # The solved configuration is saved in the job directory.
    def build(self) -> None:
        # Prevent running build if the job_directory already exists (use rebuild in that case)
        print()
        log.step(f"Solving configuration request for task {self.name}")
        if not self.get_job_directory().exists():
            log.item(f"Creating temporary directory at {self.get_job_directory()}")
            self.get_job_directory().mkdir(parents=True)
        elif (self.get_job_directory() / f"{self.name}.json").exists():
            raise StepError(
                "Job directory already built. Cannot build again over an existing build. Maybe you wanted 'rebuild'?")

        self.check_output()

        input_provides = [input.get_provides() for input in self.input]
        self.process.validate(input_provides)

        ninput_files = 0
        all_fs_used = set()
        for input in self.input:
            ninput_files += input.resolve_sample()
            all_fs_used.update(input.get_fs_set())

        if len(all_fs_used)>1:
            raise ConfigError(f"The input files for the different Inputs should be located on the same filesystem. The following filesystems are found to be used: {', '.join(all_fs_used)}")

        self.njobs, self.nfiles_per_job = self.compute_njobs(
            self.njobs, self.nfiles_per_job, ninput_files)

        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)
        self.summary()

    def check_output(self) -> None:
        if not self.output_loc.exists():
            log.item(
                f"Output directory {self.output_loc} does not exist - will be created")
        elif len(list(self.output_loc.iterdir())) == 0:
            log.item(
                f"Output directory {self.output_loc} already exists and is empty")
        else:
            log.action(
                f"Output directory {self.output_loc} already exists and is not empty!")
            log.action(
                f"Please change directory, empty it, or use the --erase option")

    # ----------------------------------------------------------------
    # Methods for Prepare step
    # ----------------------------------------------------------------
    ### Now Prepare the submission ###
    def prepare_submission(self, erase: bool = False, ignore: bool = False, job_flavour: str = "tomorrow", operating_system: str = "AlmaLinux9") -> None:
        if not self.can_modify:
            raise StepError("Cannot call prepare step. Batch already ongoing.")
        # Should reset all the job ids then
        for input in self.input:
            input.reset_inputs_job_id()

        self.create_output(erase, ignore)
        self.prepare_job_dir(job_flavour, operating_system)
        self.update_map_jobs(True)
        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)


    def create_output(self, erase: bool = False, ignore: bool = False) -> None:
        log.step("Checking output path")
        if not self.output_loc.exists():
            # Check dir exists, if not create it
            log.item(f"Creating output directory at {self.output_loc}")
            self.output_loc.mkdir()
        else:
            # If exists, check if it is empty or not
            if len(list(self.output_loc.iterdir())) > 0:
                if not erase:
                    if not ignore:
                        raise ConfigError(
                            f"Output directory at {self.output_loc} is not empty")
                else:
                    log.item(f"Deleting output directory at {self.output_loc}")
                    rmdir(self.output_loc, recurse=True)

    def prepare_job_dir(self, flavour: str, operating_system: str) -> None:
        log.step("Preparing job directory")
        if not self.get_job_directory().exists():
            # Should have been done already. Just double checking
            log.item(f"Creating temporary directory at {self.get_job_directory()}")
            self.get_job_directory().mkdir(parents=True)

        fs_used = set()
        for input in self.input:
            fs_used.update(input.get_fs_set())

        # Need to call prepare on the process
        # fs_used is either empty and we will not use it, or it contains a single element
        self.process.prepare(self.get_job_directory(), self.output_loc, self.output_what, "" if len(fs_used)==0 else list(fs_used)[0])

        # Prepare the submitter (e.g. create submission script)
        submitter = self.get_submitter()
        submitter.prepare(self.get_job_directory(), self.process.get_process_hash(), self.process.get_args(), flavour, operating_system)

    def update_map_input(self, input: inputClasses.GenericInput, from_scratch: bool) -> int:
        sample_dir = self.get_job_directory() / "input" / f"{input.get_name()}_{input.get_hash()[:7]}"
        # Empty sample_dir
        if from_scratch and sample_dir.exists():
            shutil.rmtree(sample_dir)
        chunks = input.map(self.nfiles_per_job)
        sample_dir.mkdir(parents=True, exist_ok=True)
        for ichunk, chunk in enumerate(chunks):
            ichunk += self.total_chunks ## Offset from previous samples
            chunk_file = sample_dir / f"chunk{ichunk}.txt"
            chunk_text = "\n".join([str(_) for _ in chunk])
            if len(chunk_text) > 0:
                # Write chunk files only if there is something to write
                chunk_file.write_text(chunk_text + "\n")
            [f.set_job_id(ichunk) for f in chunk]

        return len(chunks)

    def update_map_jobs(self, from_scratch: bool) -> None:
        # Map input to the jobs -> Done at build (or rebuild) and cannot be changed:
        #  - number of input/job can change on rebuild
        log.step("Preparing jobs map")
        for input in self.input:
            log.item(f"Sample: {input.get_name()}")
            self.total_chunks += self.update_map_input(input, from_scratch)

    def split_runs(self) -> None:
        if not self.can_modify:
            raise StepError("Cannot call --split-runs. Batch already ongoing.")
        log.step("Request run splitting")
        for input in self.input:
            log.item(f"Sample: {input.get_name()} -> {{0}}".format("OK" if input.split_runs() else "NOK"))


    def create_merge_jobs(self, force: bool) -> None:
        if self.merge_input is not None:
            # Already done
            return

        if not force and any(not input.is_complete() for input in self.input):
            # Samples have not all been processed
            raise StepError(f"Cannot call --merge. Some samples have not been completed yet. Try again when all samples are completed.")

        subdirs = []
        input: inputClasses.GenericInput
        for input in self.input:
            iname = input.get_name()
            input_subdirs = [f"{iname}/{v['subdir']}" for k,v in input.get_all_properties() if "subdir" in v]
            if len(input_subdirs) == 0:
                input_subdirs = [iname]
            subdirs.extend(input_subdirs)


        self.merge_input = inputClasses.MergeInput({"path": str(self.output_loc), "subdirs": subdirs})
        self.merge_process = Process()
        na62_jobs_path = Path(__file__).parent.parent.parent
        merger_path, env_path = find_root_file_merger()
        self.merge_process.load_fields({"executable": f"{na62_jobs_path}/scripts/na62_jobs_merge.sh",
                                        "env": [env_path], "template": "merge",
                                        "copy": [{"file": str(merger_path)}]})

        self.merge_process.validate([self.merge_input.get_provides()])
        self.merge_input.resolve_sample()

    def prepare_merge_jobs(self,operating_system: str) -> None:
        if self.merge_input is None or self.merge_process is None:
            # No merge job created
            return
        # Protect against doing it multiple times?
        self.merge_process.prepare(self.get_job_directory(), self.output_loc, Task.merge_output_what, list(self.merge_input.get_fs_set())[0])

        submitter = self.get_submitter()
        submitter.prepare(self.get_job_directory(), self.merge_process.get_process_hash(), self.merge_process.get_args(), "tomorrow", operating_system)
        self.update_map_input(self.merge_input, True)
        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)

    # ----------------------------------------------------------------
    # Methods for Submit step
    # ----------------------------------------------------------------
    def submit(self, job_limit: Union[int, None], batch_name: Union[str, None]) -> None:
        if self.merge_input is None or self.merge_process is None:
            which_inputs = self.input
            args_template = " ".join(f"{{{_}}}" for _ in self.process.get_args())
            unique_hash = self.process.get_process_hash()
        else:
            which_inputs = [self.merge_input]
            args_template = " ".join(f"{{{_}}}" for _ in self.merge_process.get_args())
            unique_hash = self.merge_process.get_process_hash()

        prepared_chunks = self.prepare_arguments_files(job_limit, which_inputs, args_template, unique_hash)
        if len(prepared_chunks) == 0:
            # Nothing to submit anymore
            raise StepError("No jobs to submit. All jobs have either completed successfully or are currently in progress.")

        # If no batch_name provided, use the task name
        if batch_name is None:
            batch_name = self.name
        batch_name = re.sub(r'[\W_]+', '', self.name) # remove any characters that are not numbers or letters (to be safe)
        # Always append a number to make sure we don't have twice the same name at the same time on condor
        batch_name += f"_{len(self.cluster_id)}"

        submitter = self.get_submitter()
        self.cluster_id.append((submitter.submit(self.get_job_directory(), len(prepared_chunks), batch_name, unique_hash), prepared_chunks))

        if self.merge_input is None:
            self.can_modify = False
        else:
            self.can_modify_merge = False

        log.step("Updating input status")
        for input in which_inputs:
            input.update_files_status([_["job_id"] for _ in prepared_chunks],
                                      inputClasses.InputStatus.NEEDED,
                                      inputClasses.InputStatus.RUNNING)
        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)

    def prepare_arguments_files(self, job_limit: Union[int, None], which_inputs: list, args_template: str, unique_hash: str) -> list:
        # Only the arguments file can be changed at submit (picking up different set
        # of predefined jobs):
        #  - needs to be redone several times (inital submit, resubmit failed)
        #  - need to add limiters (max jobs)
        log.step("Preparing submission arguments")

        # Add standard arguments
        args_template = "{job_id} {subdir} " + args_template
        chunk_files = []
        njobs_ready = 0
        for input in which_inputs:
            sample_dir = (self.get_job_directory() / "input" / f"{input.get_name()}_{input.get_hash()[:7]}").resolve()
            chunks = input.get_needed_chunks()
            if job_limit is not None and njobs_ready + len(chunks) > job_limit:
                allowed_chunks = job_limit - njobs_ready
                chunks = chunks[:allowed_chunks]
            for ichunk, properties in chunks:
                subdir = ""
                if "subdir" in properties:
                    subdir = "/" + properties["subdir"]

                args = {"job_id": ichunk}
                chunk_file = sample_dir / f"chunk{ichunk}.txt"
                if chunk_file.exists():
                    args["inputlist"] = chunk_file
                args.update(properties)
                args["subdir"] = f"{input.get_name()}{subdir}"
                chunk_files.append(args)
            njobs_ready += len(chunks)

        log.item("Writing arguments file")
        (self.get_job_directory() / f"arguments.{unique_hash}.txt").write_text(
            "\n".join([args_template.format(**args) for args in chunk_files]))
        return chunk_files

    def reset_failed_files(self) -> None:
        log.step("Resetting failed files")
        for input in self.input:
            input.update_files_status(None, inputClasses.InputStatus.FAILED, inputClasses.InputStatus.NEEDED)
        if self.merge_input is not None:
            self.merge_input.update_files_status(None, inputClasses.InputStatus.FAILED, inputClasses.InputStatus.NEEDED)

    def reset_gone_files(self) -> None:
        log.step("Resetting gone files")
        for input in self.input:
            input.update_files_status(None, inputClasses.InputStatus.GONE, inputClasses.InputStatus.NEEDED)
        if self.merge_input is not None:
            self.merge_input.update_files_status(None, inputClasses.InputStatus.GONE, inputClasses.InputStatus.NEEDED)

    # ----------------------------------------------------------------
    # Methods for Check step
    # ----------------------------------------------------------------
    def check(self, check_gone: bool = False) -> None:
        all_running = []
        all_completed = []
        all_batch_failed = []
        completed_batches = []
        if len(self.cluster_id) > 0:
            submitter = self.get_submitter()
            for cluster_id in self.cluster_id:
                running, completed, batch_failed = submitter.check_status(cluster_id[0], [_['job_id'] for _ in cluster_id[1]])
                all_running.extend(running)
                all_completed.extend(completed)
                all_batch_failed.extend(batch_failed)
                if len(running)==0:
                    # Batch is completed, mark as "to remove"
                    log.item(f"Batch {cluster_id[0]} has been completed")
                    completed_batches.append(cluster_id)

        for input in self.input:
            self.check_input_jobs(self.process, self.output_what, input, all_completed, all_batch_failed, check_gone)

        if self.merge_input is not None and self.merge_process is not None:
            self.check_input_jobs(self.merge_process, Task.merge_output_what, self.merge_input, all_completed, all_batch_failed, check_gone)

        # Remove batches that have completed
        [self.cluster_id.remove(_) for _ in completed_batches]

        if len(self.cluster_id) == 0:
            log.item("No more running jobs")
        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)

    def check_input_jobs(self, process: Process, output_what: list, input: inputClasses.GenericInput, all_completed: list,
                         all_batch_failed: list, check_gone: bool = False) -> None:
        recheck_jobs = []
        if check_gone:
            completed_files = input.get_files(with_status=inputClasses.InputStatus.COMPLETED)
            recheck_jobs = list(set(_.job_id for _ in completed_files))
        # Also add all the files that were previously put in unknown state
        unknown_files = input.get_files(with_status=inputClasses.InputStatus.UNKNOWN)
        recheck_jobs.extend(list(set(_.job_id for _ in unknown_files)))
        subdirs = [v["subdir"] for k,v in input.get_all_properties() if "subdir" in v]
        process_failed, process_success, process_unknown = process.check_status(all_completed + recheck_jobs, self.output_loc, output_what,
                                                                    [f"{input.get_name()}/{_}" for _ in subdirs] if len(subdirs)>0 else [input.get_name()])
        input.update_files_status(all_batch_failed+process_failed, [inputClasses.InputStatus.RUNNING, inputClasses.InputStatus.UNKNOWN], inputClasses.InputStatus.FAILED)
        input.update_files_status(process_unknown, inputClasses.InputStatus.RUNNING, inputClasses.InputStatus.UNKNOWN)
        if check_gone:
            # Also update jobs from COMPLETED state that are now missing
            input.update_files_status(process_failed, inputClasses.InputStatus.COMPLETED, inputClasses.InputStatus.GONE)
        input.update_files_status(process_success, [inputClasses.InputStatus.RUNNING, inputClasses.InputStatus.UNKNOWN], inputClasses.InputStatus.COMPLETED)

    # ----------------------------------------------------------------
    # Methods for Rebuild step
    # ----------------------------------------------------------------
    def rebuild(self):
        print()
        log.step(f"Updating configuration request for task {self.name}")

        ninput_files = 0
        all_fs_used = set()
        for input in self.input:
            ninput_files += input.resolve_sample(update=True)
            all_fs_used.update(input.get_fs_set())

        if len(all_fs_used)>1:
            raise ConfigError(f"The input files for the different Inputs should be located on the same filesystem. The following filesystems are found to be used: {', '.join(all_fs_used)}")

        log.step("Saving")
        with open(self.get_job_directory() / f"{self.name}.json", "w") as f:
            json.dump(self.save(), f)
        self.summary()

    # ----------------------------------------------------------------
    # Methods for Clear step
    # ----------------------------------------------------------------
    def clear(self):
        print()
        log.step(f"Clearing task {self.name} at {self.get_job_directory()}")

        if not self.get_job_directory().exists():
            raise ConfigError(f"Nothing to delete at {self.get_job_directory()}")

        import shutil
        shutil.rmtree(str(self.get_job_directory()))
        log.step(f"Done")

def build_task_from_json(json_file: Union[str, Path]) -> Task:
    if isinstance(json_file, str):
        json_file = Path(json_file)
    myTask = Task(save_file=json_file)

    return myTask


def process_document(yml_doc: dict) -> Task:
    found_keys = set(yml_doc.keys())
    if len(required_keys-found_keys) > 0:
        raise ConfigError(f"Missing required keys: {required_keys-found_keys}")

    myTask = Task(yml_doc)

    myTask.handle_input_section(yml_doc["input"])
    myTask.handle_output_section(yml_doc["output"])
    myTask.handle_process_section(yml_doc["process"])

    return myTask


def load_yaml(path: Union[str, Path], print_summary: bool) -> list:
    log.step(f"Reading configuration file: {path}")
    tasks = []
    with open(path, 'r') as fd:
        for doc in yaml.safe_load_all(fd.read()):
            if doc is None:
                continue
            tasks.append(process_document(doc))

    if print_summary:
        log.step(f"Found {len(tasks)} tasks")
        log.step("Summary of configuration request")
        for task in tasks:
            task.summary()

    return tasks
