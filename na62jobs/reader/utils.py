import os
import shutil
from distutils.version import LooseVersion
from pathlib import Path
from typing import Union

from .eos import EOSPath


def is_iterable(obj) -> bool:
    try:
        iter(obj)
    except TypeError:
        return False
    else:
        return True


def is_int(obj) -> bool:
    try:
        int(obj)
    except: return False
    return True

def find_root_file_merger() -> tuple:
    if "NA62ANALYSISSOURCE" in os.environ:
        na62fw_path = Path(os.environ["NA62ANALYSISSOURCE"])
        for bindir in na62fw_path.glob("bin-*"):
            if (bindir / "ROOTFileMerger").exists():
                return bindir / "ROOTFileMerger", na62fw_path / "scripts" / "env.sh"

    # Was not found in user sourced FW. Search the official samples
    fw_repo = Path("/cvmfs/na62.cern.ch/offline/NA62FW/prod/")
    latest_rev = sorted([_.name for _ in fw_repo.glob("v*.*.*")], key=LooseVersion)[-1]
    na62fw_path = fw_repo / latest_rev / "NA62Analysis"
    for bindir in na62fw_path.glob("bin-*"):
        if (bindir / "ROOTFileMerger").exists():
            return bindir / "ROOTFileMerger", na62fw_path / "scripts" / "env.sh"

    return None, None

def find_available_na62fw_env() -> Path:
    if "NA62ANALYSIS_USERDIR" in os.environ:
        na62fw_path = Path(os.environ["NA62ANALYSIS_USERDIR"])
        return na62fw_path / "scripts" / "env.sh"

    if "NA62ANALYSISSOURCE" in os.environ:
        na62fw_path = Path(os.environ["NA62ANALYSISSOURCE"])
        return na62fw_path / "scripts" / "env.sh"

    # Was not found in user sourced FW. Search the official samples
    fw_repo = Path("/cvmfs/na62.cern.ch/offline/NA62FW/prod/")
    latest_rev = sorted([_.name for _ in fw_repo.glob("v*.*.*")], key=LooseVersion)[-1]
    na62fw_path = fw_repo / latest_rev / "NA62Analysis"
    return na62fw_path / "scripts" / "env.sh"

def make_path(path: str) -> Union[Path, EOSPath]:
    if ("root:" in path) or ("/eos/" in path):
        return EOSPath(path)
    else:
        return Path(path)

def get_FS(path: Union[Path, EOSPath]) -> str:
    if isinstance(path, EOSPath):
        return "EOS"
    else:
        return "Local"

def rmdir(path: Union[Path, EOSPath], recurse: bool = False) -> None:
    if recurse:
        if isinstance(path, EOSPath):
            path.rmdir(recurse)
        else:
            shutil.rmtree(str(path))
    else:
        path.rmdir()

def copy_file(path: Union[Path, EOSPath], new_path: Union[Path, EOSPath]) -> bool:
    # 4 cases with all combination of [Path, EOSPath]
    if isinstance(path, Path):
        if isinstance(new_path, Path):
            # Easy one -> Posix to Posix
            shutil.copy(str(path), str(new_path))
        else:
            # Posix to EOS
            raise NotImplementedError("Copy from local FS to EOS not implemented")
    else:
        if isinstance(new_path, Path):
            raise NotImplementedError("Copy from EOS to local FS not implemented")
        else:
            # EOS to EOS
            path.copy(new_path)

    return True