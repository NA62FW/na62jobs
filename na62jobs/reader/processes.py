import hashlib
import re
import stat
import tarfile
from pathlib import Path
from typing import Union

from .. import logger as log
from ..exceptions import ConfigError, ConsistencyError
from . import fw_templates, payload_templates
from .eos import EOSPath
from .persistency import PersistencyMixin
from .utils import find_available_na62fw_env, get_FS


@PersistencyMixin.create_persistency(attr_to_save=["options", "dir_copy", "file_copy", "executable", "expected_files", "env", "template"],
                                     pers_version=1,
                                     attr_transform={"executable": Path,
                                                     "dir_copy": lambda v: [(Path(d[0]), d[1]) for d in v],
                                                     "file_copy": lambda v: [(Path(f[0]), f[1]) for f in v],
                                                     "expected_files": lambda v: set(re.compile(_[3:]) if "re:" in _ else _ for _ in v)
                                                     })
class Process(PersistencyMixin):
    required_fields = ["executable"]
    allowed_fields = ["options", "copy", "template", "env"]

    def __init__(self) -> None:
        self.options = []
        self.dyn_options = []
        self.dir_copy = []
        self.file_copy = []
        self.executable = Path()
        self.template = "na62_fw"
        self.expected_files = set()
        self.env = []

    def summary(self) -> str:
        cp_dir = ""
        cp_file = ""
        env_files = ""
        if len(self.dir_copy) > 0:
            cp_dir = f"{' ':>22}Folders to archive and copy:\n"
            cp_dir += "\n".join([f"{' ':>24} - {_}" for _ in self.dir_copy])
            cp_dir += "\n"
        if len(self.file_copy) > 0:
            cp_file = f"{' ':>22}Files to copy:\n"
            cp_file += "\n".join([f"{' ':>24} - {_[0]}" for _ in self.file_copy])
            cp_file += "\n"
        if len(self.env) > 0:
            env_files = f"{' ':>22}Environment files to source:\n"
            env_files += "\n".join([f"{' ':>24} - {_}" for _ in self.env])
            env_files += "\n"
        return (f"Process description:\n"
                f"{' ':>22}Executable: {self.executable} {' '.join(self.options)}\n") + cp_dir + cp_file + env_files

    def check_consistency(self, data: dict, check_consistency: bool = True) -> None:
        if check_consistency:
            if (len(self.dir_copy) != len(data["dir_copy"])) or any(str(l)!=str(r) for l,r in zip(self.dir_copy, data["dir_copy"])):
                raise ConsistencyError(f"Inconsistent directories to copy.")
            if (len(self.file_copy) != len(data["file_copy"])-1) or any(str(l[0])!=str(r[0]) for l,r in zip(self.file_copy, data["file_copy"]) if l[0]!=self.executable):
                raise ConsistencyError(f"Inconsistent files to copy.")
            if self.executable != Path(data["executable"]):
                raise ConsistencyError(f"Inconsistent executable.")

    def load(self, data: dict, check_consistency: bool = True) -> None:
        super().load(data, check_consistency)
        self.dyn_options = self.build_dyn_options()

    def build_dyn_options(self):
        matches = [re.search(r"{(\w+)}", _) for _ in self.options]
        return [_.group(1) for _ in matches if _ is not None]

    def get_payload_template(self) -> tuple:
        if hasattr(payload_templates, self.template):
            return getattr(payload_templates, self.template)
        else:
            return None, None

    def get_args(self):
        return [k for k,v in self.get_payload_template()[1].items() if v] + self.dyn_options

    # ----------------------------------------------------------------
    # Methods for Build step
    # ----------------------------------------------------------------
    def load_fields(self, process_field: dict) -> None:
        if not all(field in process_field for field in self.required_fields):
            raise ConfigError(
                f"Missing field. All the following fields are required: {self.required_fields}")
        if any((field not in self.required_fields + self.allowed_fields) for field in process_field):
            raise ConfigError(
                f"Invalid field. Only {self.required_fields + self.allowed_fields} are allowed.")

        self.executable = Path(process_field["executable"])
        if "options" in process_field and process_field["options"] is not None:
            self.options = process_field["options"].split()
            self.dyn_options = self.build_dyn_options()
        if "copy" in process_field and process_field["copy"] is not None:
            self.prepare_copy(process_field["copy"])
        if "template" in process_field and process_field["template"] is not None:
            self.template = process_field["template"]
        if "env" in process_field:
            if process_field["env"] is not None:
                if isinstance(process_field["env"], str):
                    process_field["env"] = [process_field["env"]]
                self.env = [find_available_na62fw_env() if env == "default" else Path(env) for env in process_field["env"]]
            else:
                # Env explicitely suppressed -> keep empty
                pass
        else:
            # Env not explicitely suppressed -> use na62fw available
            self.env.append(find_available_na62fw_env())

    def validate(self, samples_inputs: list) -> bool:
        # Check that the executable exists
        if not self.executable.exists():
            raise ConfigError(f"Executable {self.executable} does not exist.")
        if not self.executable.is_file():
            raise ConfigError(f"Executable {self.executable} is not a file.")
        self.file_copy.append((self.executable, self.executable.name))

        # Check that all the env files exist
        for env in self.env:
            if not env.exists():
                raise ConfigError(f"Environment file {env} does not exist.")
            if not self.executable.is_file():
                raise ConfigError(f"Environemtn file {env} is not a file.")

        # Check that all the copy inputs exist
        for copy_input, _ in self.dir_copy:
            if not copy_input.exists():
                raise ConfigError(f"Directory {copy_input} does not exist.")
            if not copy_input.is_dir():
                raise ConfigError(
                    f"Directory {copy_input} is not a directory.")
        for copy_input, _ in self.file_copy:
            if not copy_input.exists():
                raise ConfigError(f"File {copy_input} does not exist.")
            if not copy_input.is_file():
                raise ConfigError(f"File {copy_input} is not a file.")

        if not hasattr(payload_templates, self.template):
            raise ConfigError(f"Template {self.template} not found.")

        # Check what input the template requires, and compare with the list of inputs
        # from the Samples (provided as parameter)
        requirements = self.get_payload_template()[1]
        required = set(what for what, needed in requirements.items() if needed)
        opt_required = set(self.dyn_options)
        for input_name, input_req in samples_inputs:
            provided = set(what for what, needed in input_req.items() if needed)
            missing = required-provided
            if len(missing)>0:
                raise ConfigError(f"Template {self.template} requires inputs: {missing}, but this is not provided by the sample {input_name}.")
            missing = opt_required-provided
            if len(missing)>0:
                raise ConfigError(f"Provided options requires inputs: {missing}, but this is not provided by the sample {input_name}.")

        return True

    def prepare_copy(self, copy_field: list) -> None:
        allowed_inputs = ["directory", "file"]
        if any(list(ctype.keys())[0] not in allowed_inputs for ctype in copy_field):
            raise ConfigError(
                f"Invalid copy type. Only {allowed_inputs} are allowed.")
        for copy_input in copy_field:
            ctype = list(copy_input.keys())
            if len(ctype) != 1:
                raise ConfigError(
                    f"Invalid copy input. Only one copy input is allowed per item.")
            ctype = ctype[0]
            if copy_input[ctype] is None:
                raise ConfigError(f"Copy input {copy_input} is missing.")
            cpath = Path(copy_input[ctype])
            if ctype == "directory":
                self.dir_copy.append((cpath, cpath.name))
            elif ctype == "file":
                self.file_copy.append((cpath, cpath.name))

    # ----------------------------------------------------------------
    # Methods for Prepare step
    # ----------------------------------------------------------------
    def prepare(self, job_dir: Path, output_loc: Union[Path, EOSPath], output_what: list, input_system: str) -> None:
        unique_hash = self.get_process_hash()
        # Need to prepare the archives and the files to copy
        # One archive per folder
        # + one archive with all the individual files (on top)
        log.step("Preparing processing script")
        copy_path = job_dir / "copy_input"
        if not copy_path.exists():
            copy_path.mkdir(parents=True)
        self.create_dir_archive(copy_path)
        self.create_file_archive(copy_path, unique_hash)

        # Need to write the shell script
        log.item("Generating process.sh")
        process_code = self.prepare_shell_script(output_loc, output_what, unique_hash, input_system)
        process_file = job_dir / f"process.{unique_hash}.sh"
        log.subitem(f"Writing file {process_file}")
        process_file.write_text(process_code)
        process_file.chmod(process_file.stat().st_mode | stat.S_IXUSR)
        log.item("... Completed")

    def get_process_hash(self) -> str:
        return hashlib.md5(f"{self.options}{self.dyn_options}{self.executable}{self.file_copy}{self.dir_copy}".encode('utf8')).hexdigest()[:7]

    def create_file_archive(self, copy_path: Union[EOSPath, Path], unique_hash: str) -> None:
        if len(self.file_copy) > 0:
            log.item("Creating single files archive")
            with tarfile.open(str(copy_path / f"files.{unique_hash}.tgz"), "w:gz") as tar:
                for f, dest in self.file_copy:
                    tar.add(f, arcname=dest)

    def create_dir_archive(self, copy_path: Union[EOSPath, Path]) -> None:
        for folder, dest in self.dir_copy:
            log.item(f"Creating archive for directory {folder}")
            fsha = hashlib.md5(str(folder).encode("utf8")).hexdigest()[:7]
            with tarfile.open(str(copy_path / f"{folder.name}.{fsha}.tgz"), "w:gz") as tar:
                tar.add(folder, arcname=dest)

    def prepare_shell_script(self, output_loc: Union[Path, EOSPath], output_what: list, unique_hash: str, input_system: str) -> str:
        # Fetch all required templates
        #  Main
        main_template = fw_templates.main_template

        # Arguments
        arguments_template = fw_templates.arguments
        required_args = self.get_args()
        ioffset = 3 # First non-mandatory argument
        input_args = [f"{opt.upper()}=${iopt+ioffset}" for iopt, opt in enumerate(required_args)]
        arguments_template = arguments_template.format(additional_args = "\n".join(input_args))

        #  Input
        input_template = ""
        if "inputlist" in required_args:
            if "eos" in input_system:
                if input_system == "eos_na62":
                    mgm_url = "xroot://eosna62.cern.ch"
                elif input_system == "eos_user":
                    mgm_url = "xroot://eosuser.cern.ch"
                # We need to add all the input copying mechanism
                input_template = fw_templates.eos_input_copy.format(EOS_MGM_URL=mgm_url)
            else:
                # Then local or AFS. We don't care, same template
                input_template = fw_templates.local_input_copy.format()
        input_extract_template = fw_templates.extract_inputs.format(unique_hash=unique_hash)

        #  Output
        output_template = ""
        if get_FS(output_loc) == "EOS":
            log.subitem("Using template for EOS output")
            output_template = fw_templates.eos_output_copy
        else:
            raise NotImplementedError("Output on AFS not implemented")
        log.subitem("Using template for EOS input")
        output_template = output_template.format(output_what=" ".join(_.replace("re:", "") if "re:" in _ else _ for _ in output_what),
                                                 output_loc=str(output_loc), EOS_MGM_URL=output_loc.mgm_url)

        # Payload
        log.subitem(f"Using template {self.template}")
        source_str = "\n".join(f"source {env}" for env in self.env)
        payload_template = self.get_payload_template()[0]
        payload_template = payload_template.format(executable=self.executable, options=" ".join(self.options),
                                                   source_cmds=source_str)
        for opt in self.dyn_options:
            payload_template = payload_template.replace("{" + opt +"}", f"${{{opt.upper()}}}")

        # Return check
        return_check_template = fw_templates.check_return.format()

        process_code = main_template.format(arguments=arguments_template, copy_input=input_template,
                                            extract_inputs = input_extract_template,
                                            payload=payload_template, check_return = return_check_template,
                                            copy_output=output_template)
        return process_code

    # ----------------------------------------------------------------
    # Methods for Check step
    # ----------------------------------------------------------------
    def check_status(self, job_ids: list, output_loc: Union[Path, EOSPath], output_what: list, subdirs: list) -> tuple:
        log.step("Checking status of completed jobs")
        # Populate a cache of all the available files in the output directory
        log.item("Reading output directory")
        files = []
        for subdir in subdirs:
            if (output_loc / subdir).exists():
                files.extend(list(_.name for _ in (output_loc / subdir).iterdir()))

        success = []
        failed = []
        # Fail all jobs with no output at all (does not require to know what files we expect, we expect at least something)
        for job in job_ids[:]:
            job_files = set(str(_) for _ in files if str(_).split(".")[-1]==str(job))
            if len(job_files)==0:
                failed.append(job)
                # Also remove from job_ids, we won't need to check further
                job_ids.remove(job)

        if not self.expected_files:
            if not self.populate_expected_files(output_what, files):
                return failed, success, job_ids # Return unknown state
            log.item(f"Expected output files are: {', '.join(sorted(str(_) for _ in self.expected_files))}")

        for job in job_ids:
            # Check the output directory and see if all the expected files are present
            expected_files = set([f"{_}.{job}" for _ in self.expected_files if isinstance(_, str)])
            job_files = set(str(_) for _ in files if str(_).split(".")[-1]==str(job))
            if len(expected_files-job_files) == 0:
                # All fixed/wildcard files are present
                # Now check if all regex files are found
                remaining_files = job_files-expected_files
                patterns = [_ for _ in self.expected_files if isinstance(_, re.Pattern)]
                while len(patterns)>0 and len(remaining_files)>0:
                    pattern = patterns[0]
                    for file in remaining_files:
                        if pattern.match(file):
                            # OK
                            remaining_files.remove(file)
                            del patterns[0]
                            # To the next pattern
                            break

                    # This pattern has no match, no need to go further
                    break
                if len(patterns)==0:
                    # All the patterns have found a match
                    success.append(job)
                else:
                    failed.append(job)
            else:
                failed.append(job)
        return failed, success, []

    def populate_expected_files(self, output_what: list, files_cache: list) -> bool:
        log.item("Attempting to determine expected output files")
        wildcard_files = set(_ for _ in output_what if "*" in _)
        one_dyn_files = set(_ for _ in wildcard_files if "re:" in _)
        wildcard_files = wildcard_files - one_dyn_files
        one_dyn = set(re.compile(_.replace("re:", "").replace(".", "\\.").replace("*", ".*")) for _ in one_dyn_files)
        wildcard_regex = set(re.compile(_.replace(".", "\\.").replace("*", ".*")) for _ in wildcard_files)
        fixed_files = set(output_what) - wildcard_files - one_dyn_files

        # Look at the list of job ids that we are available in the output
        job_ids = [_.split(".")[-1] for _ in files_cache]

        if len(wildcard_files)==0:
            # Easy, not wildcard
            self.expected_files = fixed_files.union(one_dyn)
            return True

        # Require at least two completed jobs to cross check list of output wildcards
        if len(job_ids)<2:
            log.item("Not enough completed jobs to ensure wildcards are correctly resolved")
            return False
        # Create sets of wildcard outputs for each of the completed jobs
        # Select as reference the job with the most artefacts
        jobs_artefacts = []
        ref_job = set()
        n_max_artefacts = 0
        for job in job_ids:
            job_files = set(".".join(_.split(".")[:-1]) for _ in files_cache if _.split(".")[-1]==str(job))
            # Remove non-wildcard files
            job_files = job_files - fixed_files
            # Remove files not matching wildcard
            job_files = set(_ for _ in job_files if any(rex.match(_) is not None for rex in wildcard_regex))
            if len(job_files) > n_max_artefacts:
                ref_job = job_files
                n_max_artefacts = len(job_files)
            jobs_artefacts.append(job_files)

        # Count number of jobs with the same artefacts
        njobs_with_all = sum([len(ref_job - _) == 0 for _ in jobs_artefacts])
        if njobs_with_all < 2:
            # Unable to determine the correct set of expected files
            log.item("Not enough completed jobs with identical wildcards to correctly resolve them")
            return False

        # At least two jobs with the same - most complete - set of artefacts
        # Assume this is the correct one
        self.expected_files = fixed_files.union(ref_job).union(one_dyn)

        return True
