main_template = """#!/bin/sh

{arguments}

{copy_input}

{extract_inputs}

{payload}

{check_return}

{copy_output}
"""

arguments = """
JOBID=$1 # -> Must always be the first argument!
SUBDIR=$2 # -> Must always be the second argument!
{additional_args}
"""

extract_inputs = """
# Extract all the inputs (folders first)
for f in *.*.tgz; do
  [ -f "${{f}}" ] && tar -zxvf ${{f}}
done
# Then single files
tar -zxvf files.{unique_hash}.tgz
"""

check_return = """
# Check the successful return
ret=$?
if [ ${{ret}} != 0 ]; then
  exit ${{ret}}
fi
"""

eos_input_copy = """
# Copy input files locally from EOS
mkdir input
for f in `cat ${{INPUTLIST}}`; do
  xrdcp {EOS_MGM_URL}//${{f}} input/
done

# Create the list for the framework
ls input/* > local_input.list

# Check that we have the same number of files as the input list
if [ `cat ${{INPUTLIST}} | wc -l` != `cat local_input.list | wc -l` ]; then
  echo "Inconsistent input lists"
  exit 1
fi
"""

local_input_copy = """
# Copy input files locally from AFS/local
mkdir input
for f in `cat ${{INPUTLIST}}`; do
  cp ${{f}} input/
done

# Create the list for the framework
ls input/* > local_input.list

# Check that we have the same number of files as the input list
if [ `cat ${{INPUTLIST}} | wc -l` != `cat local_input.list | wc -l` ]; then
  echo "Inconsistent input lists"
  exit 1
fi
"""

eos_output_copy = """
# Copy the output files to the output location after appending the batch number/or sha of the batch?
found_files=""
for file in {output_what}; do
  mv ${{file}} ${{file}}.${{JOBID}}
  found_files="${{found_files}} ${{file}}.${{JOBID}}"
done

# Make sure the output directory exists
xrdfs {EOS_MGM_URL} mkdir -p {output_loc}/${{SUBDIR}}/
xrdcp ${{found_files}} {EOS_MGM_URL}/{output_loc}/${{SUBDIR}}/
"""
