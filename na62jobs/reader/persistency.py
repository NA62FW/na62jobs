import re
from enum import Enum
from pathlib import Path
from typing import Callable, Type

from ..exceptions import PersistencyError
from .eos import EOSPath

re_type = type(re.compile(""))
def to_serializable(obj):
    if isinstance(obj, Path):
        return str(obj)
    if isinstance(obj, EOSPath):
        return obj.full_str()
    elif hasattr(obj, "serialize"):
        return obj.serialize()
    elif isinstance(obj, PersistencyMixin):
        return obj.save()
    elif isinstance(obj, tuple):
        return tuple(map(to_serializable, obj))
    elif isinstance(obj, list):
        return list(map(to_serializable, obj))
    elif isinstance(obj, set):
        return list(map(to_serializable, obj))
    elif isinstance(obj, dict):
        return {to_serializable(k): to_serializable(v) for k, v in obj.items()}
    # elif isinstance(obj, re.Pattern): # Not working in python <3.7
    elif isinstance(obj, re_type):
        return f"re:{obj.pattern}"

    return obj

class PredefinedTransforms(Enum):
    DEFER = 1 # We will skip this object during loading. Will be taken care of by subclass

class PersistencyMixin:
    PersistencyMixin_attr_to_save = []
    PersistencyMixin_pers_version = 0
    PersistencyMixin_attr_transform = {}

    @staticmethod
    def _has_persistency(obj: object, class_name: str) -> bool:
        if not hasattr(obj, f"{class_name}_attr_to_save"):
            return False
        if not hasattr(obj, f"{class_name}_pers_version"):
            return False
        if not hasattr(obj, f"{class_name}_attr_transform"):
            return False
        return True

    @staticmethod
    def _build_pers_hierarchy(obj: object) -> list:
        hierarchy = []
        for parent in obj.__class__.__mro__:
            if parent == object: ## Definitely not interesting
                continue
            if PersistencyMixin._has_persistency(obj, parent.__name__):
                hierarchy.append(parent)
        return list(reversed(hierarchy))

    @staticmethod
    def create_persistency(attr_to_save: list, pers_version: int, attr_transform: dict) -> Callable:
        def _create_vars(cls: Type) -> Type:
            name = cls.__name__
            setattr(cls, f"{name}_attr_to_save", attr_to_save)
            setattr(cls, f"{name}_pers_version", pers_version)
            setattr(cls, f"{name}_attr_transform", attr_transform)
            return cls
        return _create_vars

    def save_class(self, cls: Type) -> Type:
        attributes, oversion, _ = self.get_persistency_attributes(cls.__name__)
        d = {attr: to_serializable(
            self.__dict__[attr]) for attr in attributes}
        d[f"{cls.__name__}_pers_version"] = oversion
        return d

    def save(self) -> dict:
        pers_hierarchy = PersistencyMixin._build_pers_hierarchy(self)

        data = {}
        for cls in pers_hierarchy:
            data.update(self.save_class(cls))

        return data

    def get_persistency_attributes(self, class_name: str) -> tuple:
        return (getattr(self, f"{class_name}_attr_to_save"),
                getattr(self, f"{class_name}_pers_version"),
                getattr(self, f"{class_name}_attr_transform"))

    def check_class_version(self, cls: Type, data: dict) -> None:
        pers_class = cls.__name__
        oversion = self.get_persistency_attributes(pers_class)[1]
        version_name = f"{pers_class}_pers_version"
        if version_name in data:
            iversion = data[version_name]
        else:
            iversion = 0

        if iversion < oversion:
            # Need to evolve
            cls.evolve(iversion, data)
        elif iversion > oversion:
            # Cannot happen. Software is too old
            raise PersistencyError(f"Cannot import more recent persistency for class {pers_class} (persistency version: {iversion}, software version: {oversion}).\n"
                                    "Please use a newer version of na62jobs")
        else:
            # Same persistency in input as the current class version
            pass

    def load_class(self, cls: Type, data: dict) -> None:
        attributes, _, transforms = self.get_persistency_attributes(cls.__name__)
        for k in attributes:
            if not k in data:
                raise PersistencyError(f"Cannot find field {k} in input dictionnary although it is expected by the class {cls.__name__}\n" \
                                       "Maybe it has been forgotten in schema evolution?")
            v = data[k]
            if k in transforms:
                # First deal with predefined transforms
                if transforms[k] == PredefinedTransforms.DEFER:
                    # This one means we do nothing here
                    continue
                else:
                    # Then custom ones
                    setattr(self, k, transforms[k](v))
            else:
                setattr(self, k, v)


    def load(self, data: dict, check_consistency: bool = True) -> None:
        pers_hierarchy = PersistencyMixin._build_pers_hierarchy(self)

        # Check the versions. Eventually call the evolve methods if necessary
        for cls in pers_hierarchy:
            self.check_class_version(cls, data)

        # Check the consistency of the input data
        self.check_consistency(data, check_consistency)

        # Actually load the data
        for cls in pers_hierarchy:
            self.load_class(cls, data)

    def check_consistency(self, data: dict, check_consistency: bool) -> None:
        pass

    @staticmethod
    def evolve(iversion: int, data: dict) -> None:
        """
        To be overridden by subclasses
        """

        if iversion == 0:
            # Nothing to do. No persistency existed before and we can only assume that it is compatible
            pass