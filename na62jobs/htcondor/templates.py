sub_template = """
requirements = (OpSysAndVer =?= "{operating_system}")
executable   = process.{unique_hash}.sh
output       = output/$(ClusterId)/$(jobid).out
error        = error/$(ClusterId)/$(jobid).err
log          = log/$(ClusterId).log
arguments    = $(jobid) $(subdir) {args_pass}

+JobFlavour = "{flavour}"
should_transfer_files = YES
WHEN_TO_TRANSFER_OUTPUT = ON_EXIT_OR_EVICT
+SpoolOnEvict = False

transfer_output_files   = ""
transfer_input_files = {copy_input}
queue jobid, subdir{args_input} from arguments.{unique_hash}.txt
"""