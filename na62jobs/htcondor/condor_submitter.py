from . import templates
from pathlib import Path
import subprocess
from .. import exceptions
from .. import logger as log
import re
import json
from typing import Union

class BatchSubmitter(object):
    def __init__(self) -> None:
        pass

    def prepare(self, job_dir: Path, unique_hash: str, arguments: list, flavour: str, operating_system: str) -> None:
        pass

    def submit(self, job_dir: Path, n_expected_jobs: int, batch_name: Union[str, None], unique_hash: str) -> int:
        pass
        return -1

    def check_status(self, cluster_id: int, job_ids: list) -> tuple:
        pass
        return [], [], []


def run_and_capture(command: list, print_output: bool=True, **kwargs) -> tuple:
    """Run a command while printing the live output"""
    process = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        **kwargs,
    )
    all_output = []
    while True:
        line = process.stdout.readline()
        if not line and process.poll() is not None:
            break
        txt = line.decode()
        if print_output:
            print(txt, end='')
        all_output.append(txt)

    return process.returncode, all_output


class CondorSubmitter(BatchSubmitter):
    def __init__(self) -> None:
        super().__init__()

    def prepare(self, job_dir: Path, unique_hash: str, arguments: list, flavour: str, operating_system: str) -> None:
        # Prepare the submit file
        sub_file = job_dir / f"condor.{unique_hash}.sub"
        log.step(f"Preparing submit file {sub_file}")
        log.item("Using template for HTCondor")
        # List things to copy from the input
        inputs = []
        if "inputlist" in arguments:
            inputs.append("$(inputlist)")
        inputs.extend([str(_) for _ in (job_dir / "copy_input").glob("*.tgz")])
        condor_template = templates.sub_template
        args_pass = ["$Fnx(inputlist)" if arg == "inputlist" else f"$({arg})" for arg in arguments]
        condor_template = condor_template.format(args_pass=" ".join(args_pass),
                                                 copy_input=",".join(inputs),
                                                 args_input=(", " + ", ".join(arguments)) if len(arguments)>0 else "",
                                                 flavour=flavour,
                                                 operating_system=operating_system, unique_hash=unique_hash)
        sub_file.write_text(condor_template)
        log.item("... Completed")

    def submit(self, job_dir: Path, n_expected_jobs: int, batch_name: Union[str, None], unique_hash: str) -> int:
        log.step("Submitting jobs")
        # Ensure log/error/output dirs exist
        log.item("Creating HTCondor log/error/output directories")
        (job_dir / "log").mkdir(exist_ok=True)
        (job_dir / "error").mkdir(exist_ok=True)
        (job_dir / "output").mkdir(exist_ok=True)

        # Run condor_submit (in the correct directory)
        log.item("Running condor_submit")
        condor_cmd = ["condor_submit"]
        if batch_name is not None: # use batch_name to label jobs (shows up in condor_q, internally the script uses cluster_id, this is just a label)
            log.item("Using user-specified -batch-name = {}".format(batch_name))
            condor_cmd.extend(["-batch-name", str(batch_name)])
        condor_cmd.extend([f"condor.{unique_hash}.sub"])
        ret_code, output = run_and_capture(condor_cmd, cwd=str(job_dir.absolute()))
        # Check that submission was successful: return code, number of expected jobs
        log.item("Checking condor_submit output")
        if ret_code != 0:
            raise exceptions.BatchSubmitterError("Failed to execute condor_submit command", output)
        njobs, cluster_id = self.get_njobs_and_cluster(output)
        # Create the subdirectories for the error and output for the cluster_id
        (job_dir / "error" / str(cluster_id)).mkdir(exist_ok=True)
        (job_dir / "output"/ str(cluster_id)).mkdir(exist_ok=True)
        if njobs != n_expected_jobs:
            raise exceptions.BatchSubmitterCheckFailed(f"Number of submitted jobs ({njobs}) different from number of expected jobs ({n_expected_jobs})")

        # Return the cluster_id
        return cluster_id

    def check_status(self, cluster_id: int, job_ids: list) -> tuple:
        log.step(f"Checking jobs status for cluster {cluster_id}")

        log.item("Running condor_q")
        ret_code, output = run_and_capture(["condor_q", str(cluster_id), "-nobatch", "-json"], print_output=False)
        log.item("Checking condor_q output")
        if ret_code != 0:
            raise exceptions.BatchSubmitterError("Failed to execute condor_q command", output)
        if len(output) != 0:
            json_out = json.loads("\n".join(output))
        else:
            json_out = []

        failed_jobs = []
        running_jobs = []
        action_needed_jobs = []
        completed_jobs = []

        log.item("Checking jobs status")
        # For reference: status_map = {1: "IDLE", 2: "RUNNING", 3: "REMOVED", 4: "COMPLETED", 5: "HELD", 6: "TRANSFERRING_OUTPUT", 7: "SUSPENDED"}
        failed_states = [3]
        running_states = [1, 2, 6]
        action_needed_states = [5, 7]
        completed_states = [4]
        for job in json_out:
            # proc_id = job["ProcId"]
            job_id = int(job["Args"].split()[0])
            if job_id not in job_ids:
                continue
            status = job["JobStatus"]
            if status in running_states:
                running_jobs.append(job_id)
            elif status in action_needed_states:
                action_needed_jobs.append(job_id)
            elif status in failed_states:
                failed_jobs.append(job_id)
            else:
                # Status completed: need to check if it is successful of not... Will be done outside
                completed_jobs.append(job_id)

        # Deal with the case where job is absent from condor_q -> Assume COMPLETED
        seen_jobs = set(running_jobs + action_needed_jobs + completed_jobs + failed_jobs)
        missing_jobs = list(set(job_ids) - seen_jobs)

        if len(action_needed_jobs) > 0:
            log.action("Some jobs are in HOLD: please take actions")
            log.action("Jobs with status HOLD: " + ", ".join([str(_) for _ in action_needed_jobs]))

        return running_jobs+action_needed_jobs, completed_jobs+missing_jobs, failed_jobs


    def get_njobs_and_cluster(self, output: list) -> tuple:
        for line in output:
            m = re.match(r"([0-9]+) job\(s\) submitted to cluster ([0-9]+).", line)
            if m is None:
                continue
            return int(m.group(1)), int(m.group(2))

        return None, None
