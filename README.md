[[_TOC_]]

# NA62 Batch Jobs submission system
This script is a one-entry-point to manage jobs on the CERN batch system for NA62. A simple YAML configuration file uniquely defines:
 - Your input with nice shortcuts for official NA62 data samples
 - Your process (what is running in the job)
 - Your output

The script takes care of:
 - Generating the complete list of input files
 - Grouping the input files in jobs
 - Generating the full bash script:
  - Copy of inputs to the local node (input files + NA62 standard required input + additional user-defined input)
  - Setting up the environment
  - Copy output to the destination
 - Generating the condor submit file
 - Submission of all the jobs (with possible limiters)
 - Checking the status of the jobs and ensuring coherent success
 - Resubmitting failed jobs
 - Coherently updating list of inputs and jobs if new files/samples are added
 - Possibility to process each run independently, and to merge the output files into one single file per run

# Usage
Your first task is to create the YAML config file describing your task. Please take inspiration from the examples that you can find in the [examples](examples) directory, and check the [dedicated section](#configuration-file-format) below.

Remember to source the NA62FW scripts/env.sh, see [Requirements](Requirements) below.

Once the configuration file is defined, you can pass it to the *na62_submit.py* script. The script has several commands with corresponding step, some of them can only run once the previous step has been run successfully. Each of the command has dedicated help (`-h`) in the script detailing the available options. In all cases, if your YAML config file contains multiple tasks, you must specify which tasks should be processed. All steps after **build** start from the build directory. This directory can either be specified with *--job-dir* or be extracted from the config file passed with *--config*.
 - **build**: This command is the initial step that must be run to start the process. It takes as argument the config file containing the task description. This step interprets and validates the configuration file, builds the complete list of input files and saves the resolved task into the temporary directory.
    ```
    ./na62_submit.py build [-h] [--tasks TASKS [TASKS ...]] myconfig.yml
    ```
 - **prepare** (requires **build**): This command prepares the submission to condor. It generates archives containing the files to copy on the local node, maps all the input files onto unique jobs, creates the bash script and condor submit file.  
   See the `--split-runs` option to process each run independently.  
   See the `--merge` option to generate a final set of jobs to merge the output (especially useful in combination with the `--split-runs` option)  
   See the `--flavour FLAVOUR` option to change the HTCondor job flavour (maximum duration)
   See the `--os OPERATINGSYSTEM` option to change the HTCondor OS requirement
    ```
    ./na62_submit.py prepare [-h] (--job-dir JOB_DIR | --config CONFIG) [--tasks TASKS [TASKS ...]] [--erase-outdir] [--ignore-outdir] [--split-runs] [--merge] [--flavour FLAVOUR] [--os OPERATINGSYSTEM]
    ```
 - **submit** (requires **prepare**): Do the actual submission on the batch system. It is possible to limit the number of jobs submitted (*--job-limit*). Only jobs that still needs to be submitted (not jobs that completed successfully, nor jobs currently running) are submitted. Failed (and Gone) jobs are not resubmitted without the corresponding *--reset* option. The user can specify a BATCHNAME with the *--batch_name* option, this is a label which will appear when doing condor_q (but is not used by this script), else the name of the sample will be used.
   ```
   ./na62_submit.py submit [-h] (--job-dir JOB_DIR | --config CONFIG) [--tasks TASKS [TASKS ...]] [--job-limit JOB_LIMIT] [--reset-failed] [--reset-gone] [--batch_name BATCHNAME]
   ```
 - **check** (requires **build**): Checks the status of the batch submissions. Each job can be moved in any of the following status:
   - needed: the job needs to run on the batch system
   - completed: the job has been completed successfully and the output is present
   - failed: the job has not completed successfully, or the output is not found
   - gone: the job was previously completed, but the output has disappeared (only if *--check-gone* option is used)
   ```
   ./na62_submit.py check [-h] (--job-dir JOB_DIR | --config CONFIG) [--tasks TASKS [TASKS ...]] [--check-gone]
   ```
 - **rebuild** (requires **build**): Updates the list of input files. This can be useful if:
    - The input samples for the task are not complete and new files have been added since the original **build**
    - You want to add a new sample to your task
   This command will add the new input files and create new jobs for those. Existing input files and jobs already defined in the original **build** will not be modified.
   ```
   ./na62_submit.py rebuild [-h] [--tasks TASKS [TASKS ...]] config
   ```
 - **print** (requires **build**): Prints the summary of the task.
   See the `--failed` option to print the detailed list of failed jobs (for investigating the log files).
   ```
   ./na62_submit.py print [-h] (--job-dir JOB_DIR | --config CONFIG) [--tasks TASKS [TASKS ...]] [--failed]
   ```
 - **clear** (requires **build**): Deletes the temporary directory.
   ```
   ./na62_submit.py clear [-h] (--job-dir JOB_DIR | --config CONFIG) [--tasks TASKS [TASKS ...]]
   ```

# Requirements
This script requires the NA62 environment. Please source the NA62FW scripts/env.sh before using this script.  
Additional requirements are:
  - tqdm (`python3 -m pip install --user tqdm`)

# Configuration file format
All bold items in this section (if needed) must appear as a dictionnary keyword in the configuration file.

The configuration file can contain multiple tasks that can be executed independently. Each task consists of five mandatory sections:
 - **task**: the name of the task (for identification)
 - [**input**](#input-section): definition of the input files which should be processed by the task
 - [**process**](#process-section): definition of the process itself
 - [**output**](#output-section): definition of the output of the task
 - [**temporary**](#temporary-section): temporary work directory which will be used by this system to store information about the task, and to launch the condor jobs

A short description, and link to a more complete description, of the YAML syntax is available at the [end of this document](#yaml-syntax):

## Input section
Two parameters control the number of input files per job:
 - **nfiles_per_job**: specify the number of files that you want to run per job, and the number of jobs is adapted based on the number of inputs
 - **njobs**: specify the number of jobs, and the number of input file per job is adapted based on the number of inputs.
Those parameters are mutually exclusive. They may also be ignored by some input types (e.g. "ranges").

The definition of the input files can be done in three different complementary ways:
 - [**samples**](#samples-input): Specify an official sample as input (e.g. 2021A, or k2pi)
 - [**runs**](#runs-input): Specify a list of runs as input (e.g 9001, 9002)
 - [**lists**](#lists-input): Specify a generic text file containing a list of input files
 - [**ranges**](#ranges-input): Specify a range of values to use as input for each job. 

### Samples input
This section contains a list of pre-defined samples to be used as input. Each sample is defined by:
 - **name**: name of the sample (*2016A*, *2022E*, *BeamDump2018*, *k2pi*, *k3pi*, ...)
 - **version** (optional): Reconstruction (for data) or MC (for simulation) version (e.g. *v3.1.5*, *t0.101.2*, ...), or *latest*.  
   In case of *latest*, the script will find out which is the latest version available in the EOS lists during the [build](#build-cmd) step.
   In case this parameter is ommitted, it defaults to *latest*

In addition the following parameter is required for data samples:
 - **filter**: Which filter should be used (e.g. *CTRL*, *PNN*, ...)

And the following parametes are required for MC samples:
 - **type**: Which type of MC input to use:
   - mc: standard overlaid MC
   - mc_noov: standard non-overlaid MC
   - mcdump: dump mc
   - mc_biasedov[decay]: overlaid biased MC. [decay] can be any of the overlay type, or nothing. Please see in the MC list which are available.
   - fastmc: fast MC
 - **reco_version** (optional): Similar to **version** parameter, but for the reconstruction version
 - **run_number**: run number used for the generation of the MC

For each task, you may define multiple samples to be processed (any of the above parameters can be different to define a different sample).

### Runs input
This section contains a list of runs to be used as input. Each list is defined by:
 - **numbers**: list of run numbers (e.g. 9001, 9002, 9003, ...)
 - **version**: Reconstruction version (e.g. *v3*, *v3.1*, *v3.1.5*, *t0.101.2*, ...), or *latest*, or *latest-common*.
   In case of *latest*, the script will find out, for each run, which is the latest version available in the EOS lists during the [build](#build-cmd) step.
   In case of *latest-common*, the script will stop if no common version can be found among all the run numbers listed.
 - **filter**: Which filter should be used (e.g. *CTRL*, *PNN*, ...)
 - **name** (optional): Name for identifying the sample. If not provided, will be generated as a hash.

### Lists input
This section contains a list of paths to input text files. The text files contain the list of input files to process, one file per line. Lines starting with `#` are ignored.

### Ranges input
This section contains a list of integer ranges to be used as input for the jobs. Each range is defined by three numbers: *start*, *end*, *step*. Each job will receive one of the value in the range, where the values go from *start* to *end* (both included) in steps of *step*. If not specified, *start* and *step* take by default the values 0 and 1, respectively. If only one number is specified, it is interpreted as *end*. If only two numbers are specified, it is interpreted as *start* and *end*.  
This input provides the {value} parameter.

## Process section
The process section defines the process to run in each batch job. The following parameters are available:
 - **executable**: Path to the executable to run
 - **template** (optional): One of the available template for the bash script executed by the job (currently only *na62_fw*).
   If not specified, defaults to *na62_fw*.
 - **options** (optional): Options to add to the executable (e.g. parameters for an analyser as `options: "-p Analyser:Parameter=62"`). You may use here some "template" values with the syntax '{param}', where param is the name of a parameter provided by the input type you are using. Example: the "ranges" input provides the *value* parameter, so you may use in the **options** field: '-s {value}', which will be replaced in each job by the actual parameter value for the job.
 - **copy** (optional): List of additional files and directories to copy locally on the node. Each item in the list is a single item dictionnay where the key determine the type of input. The allowed key values are:
   - **directory**: Path to a directory to import on the local node
   - **file**: Path to a file to import on the local node
 - **env** (optional): List of environment files to source in the job.  
   If the parameter is omitted, a default na62fw env.sh file will be used (either the one corresponding to the current $NA62ANALYSISSOURCE variable, or the one from the latest NA62FW release available on CVMFS). Using the value "default" will also use the same file.  
   If the parameter is present and empty, no env file will be used.

## Output section
The output section specifies what is the expected output, and where it should be placed. The following parameters are expected:
 - **location**: Path to the directory where the job outputs will be copied
 - **what**: List of files that are expected in the output. Wildcard `*` can be used, but the item must be enclosed between double-quotes (e.g. `"*.root"`).

## Temporary section
The temporary section specifies the job_dir, the home directory for the job(s). A subdirectory with the task name will be created to avoid clashes
when the same temporary directory will be specified for multiple tasks. In this directory will be: temporary files necessary for the submission 
(files to copy, arguments files, condor.sub file, bash wrapper, ...) and outputs from HTCondor (std::out, std::err and log files).
The following parameters are expected:
  - **job_dir**: Path to the home directory for the job(s).

# YAML syntax
YAML is a simple structured markdown language which is close to python datatypes. Please see [https://en.wikipedia.org/wiki/YAML](https://en.wikipedia.org/wiki/YAML) for a full description (including anchors and references) of the language. It supports multiple documents, lists and dictionaries of values, and relies on proper indentation to determine hierarchy (what contains what). One indentation is necessarily 2 spaces.
Dictionaries are created with
```
key1: value1
key2: value2
```
or
```
{key1: value1, key2: value2}
```
Lists are created with
```
- value1
- value2
```
or
```
[value1, value2]
```
In the above, `value1`, `value2` can themselves be dictionaries or lists in addition to integer, float, string values.
Different documents (tasks) are separated by
```
---
```
