# TODO
 - Allow new type of input (not file based: e.g. for NA62MC)
 - Conditions needs to be copied
# Flow

Rework of the flow:

0. Print step
  - Reads the cache and writes the summary

1. Build step
  - Read the config file
  - Validate the configuration file
  - Build the cache -> List of input files
  - Methods:
    - inputClasses: {resolve_sample}, summary, save
    - Process: {load_fields}, {validate}, prepare_copy, summary, save
    - Task: {handle_input_section}, {handle_output_section}, {handle_process_section}, {build}, check_output, summary, save

2. Prepare step -> Initial submission preparation
  - Read the cache
  - Prepare input files archives
  - Map files into jobs
  - Create shell script
  - Create condor submission file
  - Methods:
    - inputClasses: load, summary, {map}
    - Process: load, summary, {prepare}
    - Task: load, summary, load_cache, {prepare_submission}, create_output, prepare_job_dir

3. Submit step -> Submission
  - Submit

4. Check step -> Jobs checks
  - Which ones are still running, which ones are completed - successfully/failed
  - Save for each input file, whether it has been completed successfully
  - Can be rerun to check for missing files (previously successful but have been deleted)

5. Rebuild step
  - Rebuild the input files cache (new files have appeared, or new samples have been added to the config)
  - Only update previous list of input files with new files (in case of resubmit, only resubmit failed and new files)

--------------------------------
# inputClasses
  - resolve_sample: find all the input files corresponding to the input
  - summary: printing
  - save
  - load
  - map: map files into jobs (chunks)

# Process
  - load_fields: read input yaml
  - validate: verify that executable and copy inputs all exist
  - prepare_copy: read the copy section of the yaml -> called by load_fields
  - summary: printing
  - save
  - load
  - prepare: create the archives in the cache, create the bash script

# Task
  - summary: printing
  - save
  - load
  - handle_input_section: read the input section and create the input classes instances. Also the njobs/nfiles_per_job parameters
  - handle_output_section: read the output section
  - handle_process_section: read the process section and create the process instance
  - build: call check_output, process validation, resolve on the inputs, create cache directory, and save
  - load_cache: rebuild all classes from the cache save
  - check_output: check output directory (exists or not, has content or not)
  - prepare_submission: call create_output, call prepare_job_dir
  - create_output: create/delete output directory
  - prepare_job_dir: create jobs mapping, and arguments file, call process.prepare