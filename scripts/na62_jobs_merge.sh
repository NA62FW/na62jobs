#!/bin/sh

INPUTLIST=$1
OUTPUTFILE=$2

if [ $(cat ${INPUTLIST} | wc -l) -eq 1 ]; then
  cp $(head -n 1 ${INPUTLIST}) ${OUTPUTFILE}
else
  ./ROOTFileMerger ${INPUTLIST} ${OUTPUTFILE}
fi

